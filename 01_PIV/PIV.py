# -*- coding: utf-8 -*-
"""
Created on Mon Feb 21 10:04:04 2022

@author: Felbermayr Simon
"""
from __future__ import unicode_literals, print_function
from openpiv import tools, pyprocess, validation, filters, scaling
from pathlib import Path

import numpy as np
import matplotlib.pyplot as plt
#%matplotlib inline
import imageio
import cv2 
import os 
import glob
import openpiv
import ffmpeg
import sys
import time
from pathlib import Path
import pdb

def grabVideo():
    
    available=glob.glob("../../TestVideos/*.mts")+glob.glob("../../TestVideos/*.MTS")+\
        glob.glob("../../TestVideos/*.mp4")+glob.glob("../../TestVideos/*.MOV")    
    for i in available:
        print(available.index(i)+1,i)
        
    nuVid=int((input("Number of file: ")))
    
    nameVid=available[nuVid-1]
    print("Chosen video: ",nameVid)    
    
    try:
        probe = ffmpeg.probe(nameVid)
    except ffmpeg.Error as e:
        print(e.stderr, file=sys.stderr)
        sys.exit(1)
    
    video_stream = next((stream for stream in probe['streams'] if stream['codec_type'] == 'video'), None)
    if video_stream is None:
        print('No video stream found', file=sys.stderr)
        sys.exit(1)
    
    width = int(video_stream['width'])
    height = int(video_stream['height'])
    # num_frames = int(video_stream['nb_frames'])
    print('width: {}'.format(width))
    print('height: {}'.format(height))
    # print('num_frames: {}'.format(num_frames))
    args=[nameVid, width, height]
    return args
 
def editVideo(nameVid, desFPS,startTime, endTime, x, y, width, height):  
    try:
        # basename = os.path.basename(nameVid)
        filename=Path(nameVid).stem
        folder="./editedVideos/"
        
        timestr = folder+filename+".mp4" 
        #timestr=time.strftime("%Y%m%d-%H%M%S")+'.mp4'
        
        (
            ffmpeg
            .input(nameVid)
            .trim(start=startTime, end=endTime)
            .crop(x=x,y=y,width=width, height=height)
            .filter('fps', fps=desFPS, round='up')
            .metadata
            .output(timestr)
            .run()
        )
        print('\nVideo is edited and saved as ',timestr)
        return timestr
    except:
        print('Not able to edit video!')

def vid2frame(args):
    # cut frames out of video
    # args contains path were video can be found and in which folder the
    # frames should be saved
    LoadFrom, SaveToFolder =args
    # Path(path).mkdir(parents=True, exist_ok=True)
    video = cv2.VideoCapture(LoadFrom) 
    try:  
    	if not os.path.exists(SaveToFolder): 
    		os.makedirs(SaveToFolder) 
    except OSError: 
    	print ('Error: Folder cant be found or created') 
    currentframe =0
    print('######################################')
    print('Cut frames out of video started')
    while(True): 
    	ret,frame = video.read() 
    
    	if ret: 
    		name =SaveToFolder+'/frame' + str(1000+currentframe) + '.jpg'
    		#print ('Captured...' + name) 
    		cv2.imwrite(name, frame) 
    		currentframe += 1
    	else: 
    		break
    print('Cut frames out of video ended')
    
    video.release() 
    cv2.destroyAllWindows()

def frame2PIV(args):
    # calculate velocity out of different frames
    path=args
    images = len(os.listdir(path)) # check how much frames are in folder
    print('######################################')
    print('Found ',images,' frames in folder')
    print('Start PIV calculation')
    for i in range(images):
        if i>0:
            path_frame_a=path+'/frame'+str(i-1)+'.jpg'
            path_frame_b=path+'/frame'+str(i)+'.jpg'
            print(path_frame_a)
            frame_a  = tools.imread( path_frame_a )
            frame_b  = tools.imread( path_frame_b )
            # fig,ax = plt.subplots(1,2,figsize=(12,10))
            # ax[0].imshow(frame_a,cmap=plt.cm.gray)
            # ax[1].imshow(frame_b,cmap=plt.cm.gray)
            
            winsize = 32    # pixels, interrogation window size in frame A
            searchsize = 38 # pixels, search in image B
            overlap = 12    # pixels, 50% overlap
            dt = 0.038      # sec, time interval between pulses calc with frames and video time
            
            
            u0, v0, sig2noise = pyprocess.extended_search_area_piv(frame_a.astype(np.int32), 
                                                                   frame_b.astype(np.int32), 
                                                                   window_size=winsize, 
                                                                   overlap=overlap, 
                                                                   dt=dt, 
                                                                   search_area_size=searchsize, 
                                                                   sig2noise_method='peak2peak')
            
            x, y = pyprocess.get_coordinates( image_size=frame_a.shape, 
                                             search_area_size=searchsize, 
                                             overlap=overlap )
            
            
            u1, v1, mask = validation.sig2noise_val( u0, v0, 
                                                    sig2noise, 
                                                    threshold = 1.05 )
            # histogram of sig2noise
            # plt.hist(sig2noise.flatten())
            # to see where is a reasonable limit
            # filter out outliers that are very different from the
            # neighbours
            
            u2, v2 = filters.replace_outliers( u1, v1, 
                                              method='localmean', 
                                              max_iter=3, 
                                              kernel_size=3)
            # convert x,y to mm
            # convert u,v to mm/sec
            
            # calculate micrometer per pixel
            micrometer_pixel=96.52
            
            x, y, u3, v3 = scaling.uniform(x, y, u2, v2, 
                                           scaling_factor = micrometer_pixel ) # 96.52 microns/pixel
            
            # 0,0 shall be bottom left, positive rotation rate is counterclockwise
            x, y, u3, v3 = tools.transform_coordinates(x, y, u3, v3)
            #save in the ASCII table format
            showVecPlots=True
            saveVectorsToFolder="./vel_files"
            Path(saveVectorsToFolder).mkdir(parents=True, exist_ok=True)
            saveToName='/frame'+str(i-1)+'.txt'
            tools.save(x, y, u3, v3, mask, saveVectorsToFolder+saveToName)
            if (showVecPlots):
                fig, ax = plt.subplots(figsize=(8,8))
                tools.display_vector_field('exp1_001.txt', 
                                           ax=ax, scaling_factor=micrometer_pixel, 
                                           scale=50, # scale defines here the arrow length
                                           width=0.0035, # width is the thickness of the arrow
                                           on_img=True, # overlay on the image
                                           image_name= path_frame_a);
                Path("./vector_plots").mkdir(parents=True, exist_ok=True)
                fig.savefig(r'./vector_plots/piv_vel'+str(i)+'.png',bbox_inches='tight')
    print('Finished PIV calculation')
    
 
def createPIV_video():
    print('######################################')
    print('Started to create PIV video')
    #https://theailearner.com/2018/10/15/creating-video-from-images-using-opencv-python/
    frames = len(os.listdir('./vector_plots/')) # check how much frames are in folder
    img_array = []
    for i in range(frames):
    #for filename in glob.glob('./vector_plots/*.png'):
        #print(filename)
        Path("./vector_plots").mkdir(parents=True, exist_ok=True)
        filename='./vector_plots/piv_vel'+str(i+1)+'.png'
        print(filename)
        img = cv2.imread(filename)
        try:
            dimensions=img.shape
            height = img.shape[0]
            width = img.shape[1]
            channels = img.shape[2]
        except ValueError:
            print('No image was found in '+filename)
        size = (width,height)
        img_array.append(img)
    print('Finished to create PIV video')
    print('######################################')
     
    Path("./PIV_video").mkdir(parents=True, exist_ok=True)
    out = cv2.VideoWriter('./PIV_video/PIV_video.mp4',cv2.VideoWriter_fourcc(*'DIVX'), 5, size)
     
    for i in range(len(img_array)):
        out.write(img_array[i])
    out.release()
    cv2.destroyAllWindows()
    

if __name__ == '__main__':
    checkAll=False
    if(checkAll):
        available=glob.glob("../../TestVideos/*.mts")+glob.glob("../../TestVideos/*.MTS")+\
            glob.glob("../../TestVideos/*.mp4")+glob.glob("../../TestVideos/*.MOV")    
        for i in available:
            print(i)
            SaveName=editVideo(i, 10, 1, 3, 0, 0, 3840, 1620)
            saveDirect=Path(i).stem
            folder="./editedVideos/"
            savePath=folder+saveDirect
            Path(savePath).mkdir(parents=True, exist_ok=True)
            args=savePath,i
            vid2frame(args)
    else:

        # path="/home/simon/Documents/TestVideos/2022_04_22_Tests_AllSensorsCam/"
        path = "/media/simon/Elements/AvaChute/2022_06_14_FinalTests/C001C0227_20220614100145_0001/"
        videos=glob.glob(path+"*.mts")+glob.glob(path+"*.MTS")+\
            glob.glob(path+"*.mp4")+glob.glob(path+"*.MOV")

        
        for i in videos:
            name = i.split('.')
            filename = name[0].split('/')
             
            name= filename[-1]
            SaveToFolder=path+name     # define where extracted frames should be located
            LoadFrom=i
            args=LoadFrom,SaveToFolder
            # create frames out of video
            vid2frame(args)
            
            
        # grap video
        # args=grabVideo()
        
        # bestof="../../TestVideos/B005C0129_20220414160054_0001.MOV"
        # filename="B005C0129_20220414160054_0001"
        # FileType=".mp4"
        # args=bestof,3840,1620
        
        
        # # edit video
        # # editVideo(nameVid, desFPS,startTime, endTime, x, y, width, height):  
        # # SaveName=editVideo("B005C0121_20220412152347_0001.MOV", 10, 0, 1, 0, 0, args[1], args[2])
        # # path=r'./PIV.MTS'       # define path 
        # SaveToFolder='./frames_'+filename     # define where extracted frames should be located
        # LoadFrom=bestof
        # args=LoadFrom,SaveToFolder
        # # create frames out of video
        # vid2frame(args)
        
        # # create piv from defined frames
        # pathToFrames=SaveToFolder
        # # frame2PIV(pathToFrames)
        # # plt.close('all')
        # # create video from piv jpg's
        # # createPIV_video()
    
    
    
    
    
    