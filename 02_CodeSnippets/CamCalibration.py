# -*- coding: utf-8 -*-
"""
Created on Sat Apr 30 16:01:12 2022

@author: Felbermayr Simon

openCV link
https://opencv24-python-tutorials.readthedocs.io/en/stable/py_tutorials/py_calib3d/py_calibration/py_calibration.html

"""

import numpy as np
import cv2
import glob
import matplotlib.pyplot as plt
from hdf5_write import write_hdf5
from scipy.ndimage import zoom
from cv2 import aruco

plt.close('all')

plt.rcParams['text.usetex'] = True

def searchCornersChessboard(image,nameChutePart,mode,saveTo=None):
    
    # define size of chessboard
    if nameChutePart=='UpperChute':
        width=34
        length=17
    else:
        width=38
        length=17
    
    
    
    global saveMatrixTo
    
    # convert BGR to grayscale
    img=image
    gray = cv2.cvtColor(image,cv2.COLOR_BGR2GRAY)
    
    threshold=80#100
    
    # threshold picture
    gray[gray<threshold]=0
    # plt.figure()
    # plt.imshow(gray)
    
    # size of chessboard
    width = width
    length = length
    
    # termination criteria
    criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001)
    
    # prepare object points
    objp = np.zeros((length*width,3), np.float32)
    objp[:,:2] = np.mgrid[0:length,0:width].T.reshape(-1,2)*50
    
    # Arrays to store object points and image points from all the images
    objpoints = [] # 3d point in real world space
    imgpoints = [] # 2d points in image plane.
    
    
    # https://learnopencv.com/camera-calibration-using-opencv/
    ret, corners = cv2.findChessboardCorners(gray,
                                             (length,width),
                                             flags=cv2.CALIB_CB_ADAPTIVE_THRESH +
                                             cv2.CALIB_CB_FAST_CHECK +
                                             cv2.CALIB_CB_NORMALIZE_IMAGE+
                                             cv2.CALIB_CB_EXHAUSTIVE)
    
    # If found, add object points, image points (after refining them)
    if ret == True:
        print('True')
        objpoints.append(objp)
        
        # https://theailearner.com/tag/cv2-cornersubpix/
        corners2 = cv2.cornerSubPix(gray,corners,(11,11),(-1,-1),criteria)
        imgpoints.append(corners2)
    
        # # Draw and display the corners
        img = cv2.drawChessboardCorners(img, (length,width), corners2,ret)
        # cv2.imshow('img',img)
        plt.figure()
        plt.imshow(img)
        plt.xlabel(r'$l$ / pixel')
        plt.ylabel(r'$w$ / pixel')
        plt.tight_layout()
        plt.savefig('/home/simon/Documents/Thesis/3_figures/ChessboardCorners.jpg', dpi=300)
        # cv2.waitKey(500)
    
    #cv2.destroyAllWindows()
        
    # create calibration for cam
    # mtx - camera matrix
    # dist - distortion coefficient
    # rvecs - rotation vectors
    # tvecs - translation vectors
    ret, mtx, dist, rvecs, tvecs = cv2.calibrateCamera(objpoints, imgpoints, gray.shape[::-1],None,None)
    
    h,  w = img.shape[:2]
    newcameramtx, roi=cv2.getOptimalNewCameraMatrix(mtx,dist,(w,h),0,(w,h))
    
    # undistort
    dst = cv2.undistort(img, mtx, dist, None, newcameramtx)
    
    # crop the image
    # x,y,w,h = roi
    # dst = dst[y:y+h, x:x+w]
    if saveTo != None:
        SaveImageTo=saveTo+'jpg'
        cv2.imwrite(saveTo,dst)
    
    # save cam matrix, distortion and newmatrix to file
    write_hdf5(saveMatrixTo,nameChutePart,'CamMatrix',mode,mtx)
    write_hdf5(saveMatrixTo,nameChutePart,'Distortion',mode,dist)
    write_hdf5(saveMatrixTo,nameChutePart,'RefinedCamMatrix',mode,newcameramtx)
    
    # calculate size of squares in chessboard
    x_dist=[]
    y_dist=[]

    # calc corner distance
    for i in range(0,length-2):
        
        for j in range(0,width-1):
            distance_y=corners[i+1+17*j][0][1]-corners[i+17*j][0][1]
            y_dist.append(distance_y)
            
            distance_x=corners[i+17*j][0][0]-corners[i+17*(j+1)][0][0]
            x_dist.append(distance_x)
            #np.append(y_dist,[corners[i+1][0][1]-corners[i][0][1]])

    print(np.mean(x_dist))
    print(np.mean(y_dist))
    
    x_dist = np.mean(x_dist)
    y_dist = np.mean(y_dist)
    
    # plt.figure()
    # plt.subplot(121),plt.imshow(dst),plt.title('Input')
    # plt.subplot(122),plt.imshow(dst),plt.title('Output')
    # plt.show()
    
    return corners,mtx,dist,newcameramtx, x_dist, y_dist

def findArUcos(nameChutePart):
    
    if nameChutePart=='UpperChute':
        frame=cv2.imread('/home/simon/Documents/TestVideos/2022_05_12_TestswithStrobo/C001C0205_20220512140139_0001/frame1000.jpg')[:,1850:]
    else:
        frame=cv2.imread('/home/simon/Documents/TestVideos/2022_05_12_TestswithStrobo/C001C0205_20220512140139_0001/frame1000.jpg')[:,0:1850]
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    aruco_dict = aruco.Dictionary_get(aruco.DICT_6X6_250)
    parameters =  aruco.DetectorParameters_create()
    corners, ids, rejectedImgPoints = aruco.detectMarkers(gray, aruco_dict, parameters=parameters)
    frame_markers = aruco.drawDetectedMarkers(frame.copy(), corners, ids)

    plt.figure()
    plt.imshow(frame_markers)
    for i in range(len(ids)):
        c = corners[i][0]
        plt.plot([c[:, 0].mean()], [c[:, 1].mean()], "o", label = "id={0}".format(ids[i]))
    plt.legend()
    plt.show()

    # index for ArUco

    # upper chute
    # id11 upper right
    # id12 lower right
    # id9  upper left
    # id10 lower left

    # lower chute
    # id8 upper right
    # id7 lower right
    # id6 upper left
    # id5 lower left
    
    return corners,ids

def changePerspective(corners,img,nameChutePart):
    
    plt.figure()
    plt.imshow(img)
    
    global saveMatrixTo
    upperleft=corners[16,:,:]
    lowerleft=corners[0,:,:]
    upperright=corners[577,:,:]
    lowerright=corners[561,:,:]
    
    width = int(lowerright[0][1]-upperright[0][1])
    length = int(upperright[0][0]-upperleft[0][0])
    
    # define edge area of chessboard
    thresholdEdge = 200 # pixel
    ''' start ArUco method'''
    corners,ids=findArUcos(nameChutePart)
    
    if nameChutePart=='UpperChute':
        upperright=corners[np.where(ids==11)[0][0]][0][0]
        lowerright=corners[np.where(ids==12)[0][0]][0][0]
        upperleft=corners[np.where(ids==9)[0][0]][0][0]
        lowerleft=corners[np.where(ids==10)[0][0]][0][0]
    else:
        upperright = corners[np.where(ids==8)[0][0]][0][0]
        lowerright = corners[np.where(ids==7)[0][0]][0][0]
        upperleft = corners[np.where(ids==6)[0][0]][0][0]
        lowerleft = corners[np.where(ids==5)[0][0]][0][0]
    
    '''end ArUco method'''
    # define edge points of chessboard
    pts1 = np.float32([upperleft,upperright,lowerleft,lowerright])
    
    # pts2 = np.float32([[upperleft[0][0],upperright[0][1]],
    #                   [upperright[0][0],upperright[0][1]],
    #                   [upperleft[0][0],lowerright[0][1]],
    #                   [upperright[0][0],lowerright[0][1]]])
    
    pts2 = np.float32([[upperleft[0],upperright[1]],
                      [upperright[0],upperright[1]],
                      [upperleft[0],lowerright[1]],
                      [upperright[0],lowerright[1]]])
    
    # calculate transformation matrix
    M = cv2.getPerspectiveTransform(pts1,pts2)
    
    # calculate new perspective of imgage
    # dst = cv2.warpPerspective(img,M,(length+thresholdEdge,width+thresholdEdge))
    
    dst = cv2.warpPerspective(img,M,(img.shape[1],img.shape[0]))
    cv2.imwrite('newPerspective.jpg',dst)
    
    # threshLength = [length+thresholdEdge]
    # threshWidth =  [width+thresholdEdge]
    
    # write matrix to hdf5 file
    write_hdf5(saveMatrixTo,nameChutePart,'ChangePerspective','Matrix',M)
    write_hdf5(saveMatrixTo,nameChutePart,'ChangePerspective','ThresholdLength',[img.shape[1]])
    write_hdf5(saveMatrixTo,nameChutePart,'ChangePerspective','ThresholdWidth',[img.shape[0]])
    
    plt.figure()
    plt.subplot(121),plt.imshow(img),plt.title('Input')
    plt.subplot(122),plt.imshow(dst),plt.title('Output')
    plt.axhline(y=1200, color='r', linestyle='-')
    plt.show()
    
    
    return dst

def clipped_zoom(img, zoom_factor, nameChutePart, **kwargs):
    # function completly from 
    # https://stackoverflow.com/questions/37119071/scipy-rotate-and-zoom-an-image-without-changing-its-dimensions

    h, w = img.shape[:2]

    # For multichannel images we don't want to apply the zoom factor to the RGB
    # dimension, so instead we create a tuple of zoom factors, one per array
    # dimension, with 1's for any trailing dimensions after the width and height.
    zoom_tuple = (zoom_factor,) * 2 + (1,) * (img.ndim - 2)

    # Zooming out
    if zoom_factor < 1:

        # Bounding box of the zoomed-out image within the output array
        zh = int(np.round(h * zoom_factor))
        zw = int(np.round(w * zoom_factor))
        top = (h - zh) // 2
        left = (w - zw) // 2

        # Zero-padding
        out = np.zeros_like(img)
        out[top:top+zh, left:left+zw] = zoom(img, zoom_tuple, **kwargs)

    # Zooming in
    elif zoom_factor > 1:
        
        # Bounding box of the zoomed-in region within the input array
        zh = int(np.round(h / zoom_factor))
        zw = int(np.round(w / zoom_factor))
        top = (h - zh) // 2 # floor division
        left = (w - zw) // 2
        
        if nameChutePart=='Upper Chute':
            left=0
        else:
            left=left*2

        out = zoom(img[top:top+zh, left:left+zw], zoom_tuple, **kwargs)
        zoom_dim=[top, top+zh, left, left+zw, h, w, zoom_factor]
        zoom_dim=np.asarray(zoom_dim,dtype=np.float32) # print(type(zoom_dim))
        # write matrix to hdf5 file
        write_hdf5(saveMatrixTo,nameChutePart,'Zoom','Area[X,Y]',zoom_dim)
        # `out` might still be slightly larger than `img` due to rounding, so
        # trim off any extra pixels at the edges
        trim_top = ((out.shape[0] - h) // 2)
        trim_left = ((out.shape[1] - w) // 2)
        out = out[trim_top:trim_top+h, trim_left:trim_left+w]

    # If zoom_factor == 1, just return the input array
    else:
        out = img
    return out

def resizeImage(img,mean_x_dist,mean_y_dist,nameChutePart):
    
    desired_edgeLength=51
    
    percent_resize_x = (abs(mean_x_dist)/(desired_edgeLength/100))/100
    percent_resize_y = (abs(mean_y_dist)/(desired_edgeLength/100))/100
    
    resized=clipped_zoom(img,51/abs(mean_y_dist), nameChutePart)
    
    # width = int(img.shape[1] * percent_resize_x)
    # height = int(img.shape[0] * percent_resize_y)
    
    # print(width)
    
    # dim=(width,height)
    
    # resized = cv2.resize(img, dim, interpolation = cv2.INTER_AREA)
    
    return resized

def calcMatrices(img,nameChutePart):
    
    
    # calculate corners
    corners,mtx,dist,newcameramtx,x_dist,y_dist=searchCornersChessboard(image=img,
                                                          nameChutePart=nameChutePart,
                                                          mode='noPerspective',
                                                          saveTo='calibResultNoPerspective.jpg')
    '''
    start to calc back geometrical displacement

    to cite: 
        "Computer Vision: Algorithms and Applications", Richard Szeliski
        https://docs.opencv.org/4.x/da/d6e/tutorial_py_geometric_transformations.html

    '''
    dst=changePerspective(corners,img,nameChutePart)
    '''
    check again chessboard corners and save new matrix to file
    '''
    try:
    # calculate corners
        corners,mtx,dist,newcameramtx,x_dist,y_dist=searchCornersChessboard(image=dst,
                                                              nameChutePart=nameChutePart,
                                                              mode='newPerspective',
                                                              saveTo='calibResultNewPerspective.jpg')
    except:
        print('Second Calibration doesnt find corners')
    
    resized= resizeImage(dst, x_dist, y_dist, nameChutePart)
    plt.figure()
    plt.title('Resized Image')
    plt.imshow(resized)
    
    
    return corners

    
if __name__=="__main__":
    
    if 0: # create calibration matrix
        saveMatrixTo='CameraCalibration'+'.h5'
        # read image
        # /home/simon/Documents/AvalancheChute/02_CodeSnippets/CamCalibration
        images = glob.glob('./CamCalibration/*.JPG')
        upper_chute = cv2.imread(images[0])[:,1850:]#[320:1300,1849:3687]
        lower_chute = cv2.imread(images[0])[:,0:1850]#[320:1300,1849:3687]
        plt.subplot(121),plt.imshow(upper_chute),plt.title('Upper Chute')
        plt.subplot(122),plt.imshow(lower_chute),plt.title('Lower Chute')
        plt.show()
        
        corners=calcMatrices(upper_chute, 'UpperChute')
        corners=calcMatrices(lower_chute, 'LowerChute')
        
    if 1: # show chessboard corners
        image = cv2.imread('chessRAW.jpg')
        searchCornersChessboard(image,'UpperChute',mode = 'noPerspective',saveTo=None)







