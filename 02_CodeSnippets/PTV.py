

from __future__ import division, unicode_literals, print_function  # for compatibility with Python 2 and 3

import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.image as mpimg

# Optionally, tweak styles.
mpl.rc('figure',  figsize=(10, 5))
mpl.rc('image', cmap='gray')

import numpy as np
import pandas as pd
from pandas import DataFrame, Series  # for convenience

import pims
import trackpy as tp
import cv2 
import os 
import glob


# from scipy import ndimage
# from skimage import morphology, util, filters

plt.close('all')

def gray(image):
    return image[:, :, 1]  # Take just the green channel


folder="/home/simon/Documents/TestVideos/2022_04_22_Tests_AllSensorsCam/C001C0183_20220422162517_0001/data-raw/"
#"../01_PIV/frames_B005C0128_20220413143345_0001/"

# crop_area=[730:1213,2422:3158]

binary=True

frames = []
for filename in os.listdir(folder):
    img = cv2.imread(os.path.join(folder,filename),cv2.IMREAD_GRAYSCALE)
    if img is not None:
        if binary:
            # turn bw
            (thresh, im_bw) = cv2.threshold(img, 128, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)
        else:
            im_bw=img
        # crop image
        frames.append(im_bw)#[98:1462,1522:2476])
        
        
        
plt.imshow(frames[2])


diameter=1

f = tp.locate(frames[400],diameter,invert=True)
tp.annotate(f, frames[400]);



# fig, ax = plt.subplots()
# ax.hist(f['mass'], bins=20)

# # Optionally, label the axes.
# ax.set(xlabel='mass', ylabel='count');



# f = tp.locate(frames[40], diameter, invert=True, minmass=10000)
# plt.figure
# tp.annotate(f, frames[40]);



f = tp.batch(frames[:], diameter, invert=True,minmass=10,processes='auto') # minmass only for rollmeter that big


t = tp.link(f, 100, memory=1)



t1 = tp.filter_stubs(t, 10) # trajectory has to last for 25 frames 
# Compare the number of particles in the unfiltered and filtered data.
print('Before:', t['particle'].nunique())
print('After:', t1['particle'].nunique())



plt.figure()
tp.mass_size(t1.groupby('particle').mean()); # convenience function -- just plots size vs. mass


# delete negative velocity




plt.figure()
#plt.imshow(frames[len(frames)-10])
tp.plot_traj(t1,superimpose=frames[len(frames)-1]);


