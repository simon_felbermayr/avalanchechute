import matplotlib as mpl
import matplotlib.pyplot as plt

def nice_fonts(params={}):
    nice_fonts = {
        # Use LaTeX to write all text
        "text.usetex": False,
        #"font.family": "serif",
        'font.family': 'Arial',
        #'font.sans-serif': 'Verdana',
        # Use 10pt font in plots, to match 10pt font in document
        #"axes.labelsize": 8,
        "font.size": 12,
        # Make the legend/label fonts a little smaller
        #"legend.fontsize": 8,
        #"xtick.labelsize": 8,
        #"ytick.labelsize": 8,
        #"axes.titlesize":8,
    }
    nice_fonts.update(params)
    mpl.rcParams.update(nice_fonts)

def set_size(width, fraction=1, ratio=0.618, unit='pt'):
    """ Set figure dimensions to avoid scaling in LaTeX.

    Parameters
    ----------
    width: float
            Document textwidth or columnwidth in pts
    fraction: float, optional
            Fraction of the width which you wish the figure to occupy
    ratio: float, optional
            # Golden ratio to set aesthetic figure height (default 0.618)
            # (5**.5 - 1) / 2
    Returns
    -------
    fig_dim: tuple
            Dimensions of figure in inches
    """
    # Width of figure (in pts)
    fig_width_pt = width * fraction

    if unit == 'pt':
        # Convert from pt to inches
        inches_per_pt = 1 / 72.27
    elif unit == 'mm':
        inches_per_pt = 1 / 25.4

    # Figure width in inches
    fig_width_in = fig_width_pt * inches_per_pt
    # Figure height in inches
    fig_height_in = fig_width_in * ratio

    fig_dim = (fig_width_in, fig_height_in)

    return fig_dim

if __name__ == '__main__':
    nice_fonts()
    plt.figure(figsize=set_size(416,unit='pt'))
    plt.plot([1,2], [2,3])
    plt.title('test')
    plt.xlabel('test m^2')
    plt.ylabel('test \frac{m}{s}')
    plt.tight_layout()
    plt.savefig('test.png', dpi=300)