import time
import serial
import UART_def
import sys
import glob

def serial_ports():
    # check if port is available and give back feedback which port is connected
    """ Lists serial port names
        :raises EnvironmentError:
            On unsupported or unknown platforms
        :returns:
            A list of the serial ports available on the system
    """
    if sys.platform.startswith('win'):
        ports = ['COM%s' % (i + 1) for i in range(256)]
    elif sys.platform.startswith('linux') or sys.platform.startswith('cygwin'):
        # this excludes your current terminal "/dev/tty"
        ports = glob.glob('/dev/tty[A-Za-z]*')
    elif sys.platform.startswith('darwin'):
        ports = glob.glob('/dev/tty.*')
    else:
        raise EnvironmentError('Unsupported platform')

    result = []
    for port in ports:
        try:
            s = serial.Serial(port)
            s.close()
            result.append(port)
        except (OSError, serial.SerialException):
            pass
        print(result)
    return result

def connect_ZCam():
    if sys.platform.startswith('win'):
        device ='COM3'
        exists=device in serial_ports()
    elif sys.platform.startswith('linux') or sys.platform.startswith('cygwin'):
        device='/dev/ttyUSB0'
        exists=device in serial_ports()
    print('Camera is available to USB port:'+str(exists))
    try:
        ser = serial.Serial(
        	port=device,
        	baudrate=115200,
            stopbits=serial.STOPBITS_ONE,
            bytesize=serial.EIGHTBITS,
            parity=serial.PARITY_NONE
        )
        print('Camera connected')
        return ser
    except:
        print("Camera is not available try to disconnect and connect again")
        #if ser.isOpen():
        #    disconnect_ZCam(ser)
    

    
    
def disconnect_ZCam(ser):
    ser.close()
    
def write_ZCam_common_request(ser,numBytes,data):
    ser.write(serial.to_bytes([0xea,0x02,numBytes,data]))
    
def startRec(ser):
    write_ZCam_common_request(ser,0x01,UART_def.UART_START_REC)
    print("Recording started")
    
def stopRec(ser):
    write_ZCam_common_request(ser,0x01,UART_def.UART_STOP_REC)
    print("Recording stopped")
    
def decreaseISO(ser):
    ser.write(serial.to_bytes([0xea,0x02,0x03,0x2b,0x00,0x03]))

def increaseISO(ser):
    ser.write(serial.to_bytes([0xea,0x02,0x03,0x2c,0x00,0x03]))

def main():
    ser=connect_ZCam()
    startRec(ser)
    time.sleep(5)
    stopRec(ser)
    disconnect_ZCam(ser)

if __name__ == "__main__":
    main()