#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar  8 11:09:57 2022

@author: simon

description:
    In this script a video is imported and edited
        - framedropping
        - cutting slot out of full video
        - specify and cut desired area
        
information about ffmpeg-functions:
    https://kkroening.github.io/ffmpeg-python/
"""
from __future__ import unicode_literals, print_function
import ffmpeg
import os
import glob
import sys
import time

def grabVideo():
    # available=glob.glob("*.mts")+glob.glob("*.MTS")+\
    #     glob.glob("*.mp4")+glob.glob("*.MOV")
        
    # for i in available:
    #     print(available.index(i)+1,i)
        
    # nuVid=int((input("Number of file: ")))
    
    # nameVid=available[nuVid-1]
    
    nameVid="B005C0147_20220415123256_0001.MOV"
    print("Chosen video: ",nameVid)    
    
    try:
        probe = ffmpeg.probe(nameVid)
    except ffmpeg.Error as e:
        print(e.stderr, file=sys.stderr)
        sys.exit(1)
    
    video_stream = next((stream for stream in probe['streams'] if stream['codec_type'] == 'video'), None)
    if video_stream is None:
        print('No video stream found', file=sys.stderr)
        sys.exit(1)
    
    width = int(video_stream['width'])
    height = int(video_stream['height'])
    # num_frames = int(video_stream['nb_frames'])
    print('width: {}'.format(width))
    print('height: {}'.format(height))
    # print('num_frames: {}'.format(num_frames))
    args=[nameVid, width, height]
    return args
 
def editVideo(nameVid, desFPS,startTime, endTime, x, y, width, height):  
    try:       
        timestr = time.strftime("%Y%m%d-%H%M%S")+'.mp4'
        (
            ffmpeg
            .input(nameVid)
            .trim(start=startTime, end=endTime)
            .crop(x=x,y=y,width=width, height=height)
            .filter('fps', fps=desFPS, round='up')
            .output(timestr)
            .run()
        )
        print('\nVideo is edited and saved as ',timestr)
    except:
        print('Not able to edit video!')
        
def getMetadata(nameVid): 
    os.system('ffmpeg -i '+nameVid +' -f ffmetadata ' +nameVid+'metadata.txt')

if __name__ == '__main__':
    # args=grabVideo()
    # path="/home/simon/Documents/TestVideos/TestLasersAppearance/"
    # videos=["B005C0144_20220415122026_0001.MOV",
    #         "B005C0145_20220415122918_0001.MOV",
    #         "B005C0146_20220415123231_0001.MOV",
    #         "B005C0147_20220415123256_0001.MOV"]
    
    
    
    
    # path="/home/simon/Documents/TestVideos/2022_04_22_Tests_AllSensorsCam/"
    # videos=glob.glob(path+"*.mts")+glob.glob(path+"*.MTS")+\
    #     glob.glob(path+"*.mp4")+glob.glob(path+"*.MOV")

    # videos="/home/simon/Documents/AvalancheChute/02_CodeSnippets/C001C0197_20220504134032.JPG"
    # for i in videos:
    #     getMetadata(i)

    #getMetadata(args[0], 25, 0, 10, 0, 0, args[1], args[2])
    
    videos="/home/simon/C001C0205_20220512140139_0001.MOV"
    
    args=3840,1620
    editVideo(videos, 25, 0, 10, 0, 0, args[0], args[1])
        
    
    

