# -*- coding: utf-8 -*-
"""
Created on Sun May  1 10:06:31 2022

@author: Felbermayr Simon
"""

import numpy as np
import cv2
import glob
import matplotlib.pyplot as plt

plt.close('all')

# read image
images = glob.glob('./calibresult.jpg')
img=cv2.imread(images[0])

plt.figure()
plt.imshow(img)

upperleft=corners[17,:,:]
lowerleft=corners[0,:,:]
upperright=corners[577,:,:]
lowerright=corners[561,:,:]

pts1 = np.float32([[56,65],[368,52],[28,387],[389,390]])
pts2 = np.float32([[0,0],[300,0],[0,300],[300,300]])
M = cv2.getPerspectiveTransform(pts1,pts2)
dst = cv2.warpPerspective(img,M,(300,300))
plt.subplot(121),plt.imshow(img),plt.title('Input')
plt.subplot(122),plt.imshow(dst),plt.title('Output')
plt.show()