# https://nanonets.com/blog/optical-flow/
from multiprocessing import Pool
import os
import numpy as np
import cv2 as cv
import glob
import matplotlib.pyplot as plt
from matplotlib import rc
from hdf5_write import write_hdf5
import hdf5_write
import json

plt.close('all')

plt.rcParams['text.usetex'] = True

def optical_flow_img_pair(img1, img2, save_fig=True, param={}):
    # from example: https://github.com/opencv/opencv/blob/4.x/samples/python/opt_flow.py
    # nice explanations: https://www.youtube.com/watch?v=4v_keMNROv4
    # https://stackoverflow.com/questions/47732102/unknown-output-of-opencvs-calcopticalflowfarneback
    
    global folder
    global saveToSubfolder
    
    fps=160
    T=1/fps
    
    name1=img1.split('.')
    filename1 = name1[0].split('/')
    
    name=img2.split('.')
    filename = name[0].split('/')
    
    if isinstance(img1, str):
        # load if img1 is a filename, otherwith expect the return of read_image()
        img1 = read_image(img1)
    if isinstance(img2, str):
        # dd_out = img2.replace('-raw', '') # generate output folder, e.g. ~/data-raw/.../img.jpg -> ~/data/.../img.jpg
        savefig_type='.png'
        dd_out = folder +saveToSubfolder+filename[-1]+savefig_type
        img2 = read_image(img2) # img2 is a filename
        
    flow = None
    
    parameter = {
        'pyr_scale': 0.5,
        'levels': 5,
        'winsize': 4,
        'iterations': 3,
        'poly_n': 7,
        'poly_sigma': 1.2,
        'flags': 0,
        }
    parameter.update(param)
    
    flow = cv.calcOpticalFlowFarneback(img1, img2, flow, parameter['pyr_scale'], parameter['levels'],parameter['winsize'], parameter['iterations'], parameter['poly_n'], parameter['poly_sigma'], parameter['flags'])
    if filename[-2]=='02_UpperChute':
        # filter region of interest
        y1=348
        y2=1335
        
        x1=0
        x2=1930
        # poi=np.array([1000,830]) # point for pulsed light´
        poi=np.array([500,815]) # point of OSMextracted dat
        mask=np.zeros(flow[:,:,0].shape[:2],np.uint8)
        mask[y1:y2,:]=1
        # print("Upper Chute")
        flow = cv.bitwise_and(flow,flow,mask = mask)
        
    else:
        # filter region of interest
        y1=348
        y2=1335
        
        x1=0
        x2=1840
        
        poi=np.array([500+150,815])
        mask=np.zeros(flow[:,:,0].shape[:2],np.uint8)
        mask[y1:y2,:]=1
        
        flow = cv.bitwise_and(flow,flow,mask = mask)
        

    
    
    # calc timestamp
    dt1 = int(filename1[-1].replace('frame',''))
    dt2 = int(filename[-1].replace('frame',''))
    
    # frames per second ZCam
    fps = 160
    T = 1/fps
    
    time_diff=[(dt2-dt1)*T]
    print(time_diff)
    
    flow = flow/1000/time_diff # get m/s
    v = np.sqrt(flow[:, :, 0] * flow[:, :, 0] + flow[:, :, 1] * flow[:, :, 1])
    
    fnh5=folder +saveToSubfolder + filename[-3] + '.h5'
    
    write_hdf5(fnh5,'vx','None',filename[-1],flow[:, :, 0])
    write_hdf5(fnh5,'vy','None',filename[-1],flow[:, :, 1])
    write_hdf5(fnh5,'v','None',filename[-1],v)
    write_hdf5(fnh5,'timestamp','None',filename[-1],time_diff)
    
    # set title for subplots
    timestamp=str(round((dt2-1000)*T*1000,5)) # -1000 because save names; *1000 to have ms
    
    if save_fig:
        #plt.figure()
        fig, axs = plt.subplots(2, 2)
        plt.setp(axs, xticks=np.arange(0, img2.shape[1], 400), yticks=np.arange(0, img2.shape[0], 400))
        
        fig.suptitle(r'Timestamp: '+timestamp+' ms', fontsize=16)
        axs[0, 0].set_title(r'a) Grayscale Image')
        axs[1, 0].set_title(r'c) Horizontal Velocity')
        axs[1, 1].set_title(r'd) Vertical Velocity')
        axs[0, 1].set_title(r'b) Absolute Velocity')
        
        # image
        sub1=axs[0,0].imshow(img2[:,150:], cmap='gray', vmin=0, vmax=255, aspect='auto')
        axs[0,0].plot(poi[0],poi[1],'ro')
        axs[0,0].set_xlabel(r'$l$ / mm')
        axs[0,0].set_ylabel(r'$w$ / mm')
        plt.colorbar(sub1,ax=axs[0,0], label='$i_{gray}$ / 1')
        #axs[0,0].xticks(np.arange(0, img2.shape[1], 500))
        # vx component
        #axs[1].imshow(flow[:, :, 0] * (-1), vmin=0, vmax=5)
        sub2=axs[1,0].imshow(flow[:, 150:, 0], cmap='seismic', vmin=-5, vmax=5, aspect='auto')
        axs[1,0].set_xlabel(r'$l$ / mm')
        axs[1,0].set_ylabel(r'$w$ / mm')
        plt.colorbar(sub2,ax=axs[1,0], label='$v_x$ / m/s')
        #plt.xticks(np.arange(0, img2.shape[1], 500))
        #bar=plt.colorbar(axs[1,0])
        # vy component
        sub3=axs[1,1].imshow(flow[:, 150:, 1], cmap='seismic', vmin=-5, vmax=5, aspect='auto')
        axs[1,1].set_xlabel(r'$l$ / mm')
        axs[1,1].set_ylabel(r'$w$ / mm')
        plt.colorbar(sub3,ax=axs[1,1], label='$v_y$ / m/s')
        #plt.xticks(np.arange(0, img2.shape[1], 500))
        #lt.colorbar()
        # v abs
        sub4=axs[0,1].imshow(v[:,150:], cmap='viridis', vmin=0, vmax=6, aspect='auto')
        axs[0,1].set_xlabel(r'$l$ / mm')
        axs[0,1].set_ylabel(r'$w$ / mm')
        plt.colorbar(sub4,ax=axs[0,1], label='$v_{abs}$ / m/s')
        #plt.xticks(np.arange(0, img2.shape[1], 500))
        
        # set plot settings
    
        plt.subplots_adjust(left=None, bottom=None, right=None, top=None, wspace=0.7, hspace=0.5)
        
        plt.savefig(dd_out, dpi=700)
        plt.close()
    
    return flow
    

def optical_flow_loop_files(fns, start_idx=0, end_idx=None):
    if end_idx is None:
        end_idx = len(fns)
    prevgray = read_image(fns[start_idx])
    for nr in range(start_idx + 1, end_idx):
        print('frame:', os.path.split(fns[nr])[1])
        gray = read_image(fns[nr])
        optical_flow_img_pair(prevgray, gray)
        prevgray = gray

        # if 1:
        #     fig.savefig(fns[nr].replace('-raw', ''))
        # else:
        #     plt.show()

def read_image(fn):

    d1 = cv.imread(fn)

    if 0:
        # BALL: for frames_B005C0103_20220407150600_0001
        # crop values
        d1 = d1[700:1000, :, :]
        # for the ball: merge color channels with strong weighting of ball's red
        B, G, R = cv.split(d1)
        img = -0.7 * B - 0.7 * G + 1.2 * R  # e.g. from gimp color->components->mono mixer
        img[img < 0] = 0  # crop all negative values
        max_red_value = 50  # somewhere near img.max() but hard coded to keep the value over the frames
        img = img / max_red_value * 255  # scale to 0-255
        img[img > 255] = 255  # crop all negative values
    else:
        # TAPE: for frames_B005C0121_20220412152347_0001
        # crop values
        #d1 = d1[900:1200, :, :]
        # for high contrast: simple conversion
        img = cv.cvtColor(d1, cv.COLOR_BGR2GRAY)

    return img

if __name__ == '__main__':
    
    setup1 = {
        'pyr_scale': 0.7,
        'levels': 1,
        'winsize': 4,
        'iterations': 3,
        'poly_n': 7,
        'poly_sigma': 1,
        'flags': 0,
        }
    
    setup2 = {
        'pyr_scale': 0.7,
        'levels': 10,
        'winsize': 4,
        'iterations': 3,
        'poly_n': 7,
        'poly_sigma': 1,
        'flags': 0,
        }
    setup3 = {
        'pyr_scale': 0.7,
        'levels': 10,
        'winsize': 2,
        'iterations': 3,
        'poly_n': 7,
        'poly_sigma': 1,
        'flags': 0,
        }
    setup4 = {
        'pyr_scale': 0.7,
        'levels': 10,
        'winsize': 4,
        'iterations': 3,
        'poly_n': 5,
        'poly_sigma': 1,
        'flags': 0,
        }
    setup5 = {
        'pyr_scale': 0.7,
        'levels': 10,
        'winsize': 8,
        'iterations': 3,
        'poly_n': 7,
        'poly_sigma': 1,
        'flags': 0,
        }
    setup6 = {
        'pyr_scale': 0.7,
        'levels': 10,
        'winsize': 16,
        'iterations': 3,
        'poly_n': 7,
        'poly_sigma': 1,
        'flags': 0,
        }
    if 0: # evaluate whole folder
        folder='/media/simon/radar_server/AvaChute/2022_06_14_FinalTests/C001C0225_20220614095036_0001/02_UpperChute/'
        saveToSubfolder = 'Setup_6/'
        path = folder+"*.jpg"
        
        fns = sorted(glob.glob(path))
        
        start_idx = 0
        end_idx = 650
        
        parameter = setup6
        
        name=fns[0].split('.')
        filename = name[0].split('/')
        fnh5 = folder +saveToSubfolder
        
        if not os.path.exists(fnh5):
            os.makedirs(fnh5)
        
        # write parameters to text file for quick info
        fnparams = fnh5 + 'params.txt'
        with open(fnparams, 'w') as file:
             file.write(json.dumps(parameter)) # use `json.loads` to do the reverse
        
        fnh5 = fnh5+ filename[-3] + '.h5'
        hdf5_write.hdf5_write_dict(filename=fnh5, dict_name='parameter', dictionary=parameter)
        
        # p = hdf5_write.hdf5_load_dict(fnh5, dict_name='parameter')
    
        for i in range(start_idx,end_idx-1):
            # name = fns[i].split('.')
            # filename = name[0].split('/')
             
            # SaveToFolder=fns[i].replace('-raw', '-hp-filtered')
            img_1=fns[i]
            img_2=fns[i+1]
            flow=optical_flow_img_pair(img_1,img_2,save_fig=True, param=parameter)
            
        print('Done')
        
    if 0: # check for maximal velocity
        img_1 = '/media/simon/Elements/AvaChute/2022_06_14_FinalTests/C001C0225_20220614095036_0001/02_UpperChute/frame1080.jpg'
        img_2 = '/media/simon/Elements/AvaChute/2022_06_14_FinalTests/C001C0225_20220614095036_0001/02_UpperChute/frame1119.jpg'
        
        folder='/media/simon/Elements/AvaChute/2022_06_14_FinalTests/C001C0225_20220614095036_0001/02_UpperChute/'
        saveToSubfolder = 'Setup_6/'
        path = folder+"*.jpg"
        
        fns = sorted(glob.glob(path))
        
        start_idx = 80
        end_idx = 150
        

        
        parameters = [setup1, setup2, setup3, setup4, setup5, setup6]
        
        parameter = setup6
            
        name=fns[0].split('.')
        filename = name[0].split('/')
        fnh5 = folder +saveToSubfolder
        
        if not os.path.exists(fnh5):
            os.makedirs(fnh5)
        
        # write parameters to text file for quick info
        fnparams = fnh5 + 'params.txt'
        with open(fnparams, 'w') as file:
             file.write(json.dumps(parameter)) # use `json.loads` to do the reverse
        
        fnh5 = fnh5+ filename[-3] + '.h5'
        hdf5_write.hdf5_write_dict(filename=fnh5, dict_name='parameter', dictionary=parameter)
        
        # preallocate matrix
        n = 200
        flow_setup1 = np.zeros((n,2))
        flow_setup2 = np.zeros((n,2))
        flow_setup3 = np.zeros((n,2))
        flow_setup4 = np.zeros((n,2))
        flow_setup5 = np.zeros((n,2))
        flow_setup6 = np.zeros((n,2))
        k=0
        
        for i in range(start_idx,end_idx-1,2):
            # name = fns[i].split('.')
            # filename = name[0].split('/')
             
            # SaveToFolder=fns[i].replace('-raw', '-hp-filtered')
            img_1=fns[start_idx]
            img_2=fns[i+1]
            flow = optical_flow_img_pair(img_1,img_2,save_fig=False, param = parameter)
                
            # flow_setup1[k,:] = optical_flow_img_pair(img_1,img_2,save_fig=False, param=setup1)
            # flow_setup2[k,:] = optical_flow_img_pair(img_1,img_2,save_fig=False, param=setup2)   
            # flow_setup3[k,:] = optical_flow_img_pair(img_1,img_2,save_fig=False, param=setup3)   
            # flow_setup4[k,:] = optical_flow_img_pair(img_1,img_2,save_fig=False, param=setup4)   
            # flow_setup5[k,:] = optical_flow_img_pair(img_1,img_2,save_fig=False, param=setup5)   
            # flow_setup6[k,:] = optical_flow_img_pair(img_1,img_2,save_fig=False, param=setup6)   
                
    
    
