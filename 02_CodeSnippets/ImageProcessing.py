#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr 15 10:26:43 2022

@author: simon
"""

import matplotlib.pyplot as plt
import matplotlib.patches as mpatches

from skimage import data
from skimage.filters import threshold_otsu
from skimage.segmentation import clear_border
from skimage.measure import label, regionprops
from skimage.morphology import closing, square
from skimage.color import label2rgb
import cv2
import os
import numpy as np
import skimage.io
import glob
from skimage.color import rgb2gray


def highPassFilt(LoadFrom,SaveToFolder):
    
    
    img=cv2.imread(LoadFrom)[429:1207,1855:3806]
    
    # read input and convert to grayscale
    #img = cv2.imread('lena_gray.png', cv2.IMREAD_GRAYSCALE)
    #img = cv2.imread(path)
    
    # do dft saving as complex output
    dft = np.fft.fft2(img, axes=(0,1))
    
    # apply shift of origin to center of image
    dft_shift = np.fft.fftshift(dft)
    
    # generate spectrum from magnitude image (for viewing only)
    mag = np.abs(dft_shift)
    spec = np.log(mag) / 20
    
    # create white circle mask on black background and invert so black circle on white background
    radius = 32
    mask = np.zeros_like(img)
    cy = mask.shape[0] // 2
    cx = mask.shape[1] // 2
    cv2.circle(mask, (cx,cy), radius, color = (255,255,255),thickness = -1)[0]
    mask = 255 - mask
    
    # blur the mask
    mask2 = cv2.GaussianBlur(mask, (19,19), 0)
    
    # apply mask to dft_shift
    dft_shift_masked = np.multiply(dft_shift,mask) / 255
    dft_shift_masked2 = np.multiply(dft_shift,mask2) / 255
    
    
    # shift origin from center to upper left corner
    back_ishift = np.fft.ifftshift(dft_shift)
    back_ishift_masked = np.fft.ifftshift(dft_shift_masked)
    back_ishift_masked2 = np.fft.ifftshift(dft_shift_masked2)
    
    
    # do idft saving as complex output
    img_back = np.fft.ifft2(back_ishift, axes=(0,1))
    img_filtered = np.fft.ifft2(back_ishift_masked, axes=(0,1))
    img_filtered2 = np.fft.ifft2(back_ishift_masked2, axes=(0,1))
    
    # combine complex real and imaginary components to form (the magnitude for) the original image again
    # multiply by 3 to increase brightness
    img_back = np.abs(img_back).clip(0,255).astype(np.uint8)
    img_filtered = np.abs(3*img_filtered).clip(0,255).astype(np.uint8)
    img_filtered2 = np.abs(3*img_filtered2).clip(0,255).astype(np.uint8)
    
    
    # cv2.imshow("ORIGINAL", img)
    # cv2.imshow("SPECTRUM", spec)
    # cv2.imshow("MASK", mask)
    # cv2.imshow("MASK2", mask2)
    # cv2.imshow("ORIGINAL DFT/IFT ROUND TRIP", img_back)
    # cv2.imshow("FILTERED DFT/IFT ROUND TRIP", img_filtered)
    # cv2.imshow("FILTERED2 DFT/IFT ROUND TRIP", img_filtered2)
    # cv2.waitKey(0)
    # cv2.destroyAllWindows()
    # cv2.imwrite("lena_dft_numpy_highpass_filtered2.png", img_filtered2)
    
    cv2.imwrite(SaveToFolder, img_filtered2)
    


if True:
    path="/home/simon/Documents/TestVideos/2022_04_22_Tests_AllSensorsCam/C001C0177_20220422160244_0001/data-raw/"
    frames=glob.glob(path+"*.jpg")

    start_idx=400
    end_idx=500
    for i in range(start_idx,end_idx):
        name = frames[i].split('.')
        filename = name[0].split('/')
         
        SaveToFolder=frames[i].replace('-raw', '-hp-filtered')
        LoadFrom=frames[i]
        highPassFilt(LoadFrom,SaveToFolder)


if False: # code optimised for orange ball
    folder="../01_PIV/frames_B005C0103_20220407150600_0001/"
    filename="frame950.jpg"
    
    data=folder+filename

    image = cv2.imread(data)[418:1226,1867:3726]#,cv2.IMREAD_GRAYSCALE)[418:1226,1867:3726]
    
    # specific for orange ball to track
    reddishch0 = image[:, :, 0]<170
    reddishch1 = image[:, :, 1]<170
    reddishch2 = image[:, :, 2]>170
    reddish=reddishch0*reddishch1*reddishch2
    image[reddish] = [0, 0, 1]
    image[reddish] = [0, 0, 2]
    image[reddish] = [0, 0, 3]
    
    plt.imshow(image)
    
    	
    image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    
    # apply threshold
    thresh = threshold_otsu(image)
    bw = closing(image > thresh, square(3))
    
    # remove artifacts connected to image border
    cleared = clear_border(bw)
    
    # label image regions
    label_image = label(cleared)
    # to make the background transparent, pass the value of `bg_label`,
    # and leave `bg_color` as `None` and `kind` as `overlay`
    image_label_overlay = label2rgb(label_image, image=image, bg_label=0)
    
    fig, ax = plt.subplots(figsize=(10, 6))
    ax.imshow(image_label_overlay)
    
    for region in regionprops(label_image):
        # take regions with large enough areas
        if region.area >= 100:
            # draw rectangle around segmented coins
            minr, minc, maxr, maxc = region.bbox
            rect = mpatches.Rectangle((minc, minr), maxc - minc, maxr - minr,
                                      fill=False, edgecolor='red', linewidth=2)
            ax.add_patch(rect)
    
    ax.set_axis_off()
    plt.tight_layout()
    plt.show()