# -*- coding: utf-8 -*-
"""
Created on Mon Feb 21 11:12:14 2022

@author: Felbermayr Simon
"""
from __future__ import unicode_literals, print_function

import numpy as np
import matplotlib.pyplot as plt
#%matplotlib inline
import math 
import os

import skimage.io
from skimage.color import rgb2gray
import numpy_indexed as npi
import cv2
import glob
from multiprocessing import Pool

import sys
# insert at 1, 0 is the script path (or '' in REPL)
sys.path.insert(1, '/home/simon/Documents/AvalancheChute/00_DAQ')

import DataEval
import nice_figures as nf

# plt.close('all')
plt.rcParams['text.usetex'] = True

def calcLaserTrianguation(img ,save=False, angle = 0, offset = 0, lower1=[0, 90, 20], upper1=[255, 120, 255], lower2 = [155, 16, 92], upper2 = [179, 255, 255], mode = 'hsv'):
    
    # mode='hsv'   # 'hsv' 'grayThresh' 'canny' 'fft'        
    mask_hsv_low = True
    if mode == 'grayThresh':
        
        # img = skimage.io.imread(path)#[431:1207,1240:3049]#[631:1018,1407:1808]
        
        grayscale = rgb2gray(img)
        
        size=grayscale.shape
        posMax=[]
        
        # valMax=np.amax(grayscale, axis=0)
        # for i in len(valMax):
        #     posMax.append(np.where(grayscale[:,i]==valMax[i]))
        
        
        # plt.figure()
        # plt.imshow(grayscale)
        ''''Filter laser line from background'''
        grayscale[grayscale>0.23]=255
        grayscale[grayscale<0.23]=0
        
        # plt.figure()
        # plt.imshow(grayscale)
        
        final=grayscale
        
        
        
        
    elif mode == 'fft':
        
        # img = skimage.io.imread(path)
        # render=img[:,:,0]>160
        # img[render]=[255,255,255]
        # img[~render]=[0,0,0]
        
        grayscale = rgb2gray(img)
        
        
        
        
        dark_image_grey_fourier = np.fft.fftshift(np.fft.fft2(grayscale))
        
        # plt.figure(num=None, figsize=(8, 6), dpi=80)
        # plt.imshow(np.log(abs(dark_image_grey_fourier)), cmap='gray');
        '''Getting rid of noise in image'''
        i=1
        f_size=15
        
        # find DC part and whipe out 10 % around it to get rid of high freq signals
        dcPoint=np.where(dark_image_grey_fourier == np.amax(dark_image_grey_fourier))
        
        sizeGrayscale=dark_image_grey_fourier.shape
        
        filtPercent=0.5 # %
        filtPixels=int(np.floor(sizeGrayscale[0]*filtPercent/100))
        
        start=dcPoint[0]-filtPixels
        end=dcPoint[0]+filtPixels
        
        dark_image_grey_fourier[start[0]:end[0], :int(np.ceil(sizeGrayscale[1]/2))] = i
        dark_image_grey_fourier[start[0]:end[0],-int(np.ceil(sizeGrayscale[1]/2)):] = i
        
        fig, ax = plt.subplots(1,3,figsize=(15,15))
        ax[0].imshow(np.log(abs(dark_image_grey_fourier)), cmap='gray')
        ax[0].set_title('Masked Fourier', fontsize = f_size)
        ax[1].imshow(rgb2gray(img), cmap = 'gray')
        ax[1].set_title('Greyscale Image', fontsize = f_size);
        ax[2].imshow(abs(np.fft.ifft2(dark_image_grey_fourier)), 
                         cmap='gray')
        ax[2].set_title('Transformed Greyscale Image', 
                         fontsize = f_size);
        
        img_fft=abs(np.fft.ifft2(dark_image_grey_fourier))
        # img_fft_filt=[]
        
        ''''Filter laser line from background'''
        img_fft[img_fft>0.23]=255
        img_fft[img_fft<0.23]=0
        
        plt.figure()
        plt.imshow(img_fft)
        
        final=img_fft
    
    elif mode == 'fft_hp':
        
        
        mask_hsv_low = False
        
        dft = np.fft.fft2(img, axes=(0,1))
        
        dft_shift = np.fft.fftshift(dft)
        
        mag = np.abs(dft_shift)
        spec = np.log(mag) / 20
        
        radius = 1
        mask = np.zeros_like(img)
        cy = mask.shape[0] // 2
        cx = mask.shape[1] // 2
        cv2.circle(mask, (cx,cy), radius, (255,255,255), -1)[0]
        mask = 255 - mask
        
        mask2 = cv2.GaussianBlur(mask, (19,19), 0)
        
        dft_shift_masked = np.multiply(dft_shift,mask) / 255
        dft_shift_masked2 = np.multiply(dft_shift,mask2) / 255
        
        back_ishift = np.fft.ifftshift(dft_shift)
        back_ishift_masked = np.fft.ifftshift(dft_shift_masked)
        back_ishift_masked2 = np.fft.ifftshift(dft_shift_masked2)
        
        img_back = np.fft.ifft2(back_ishift, axes=(0,1))
        img_filtered = np.fft.ifft2(back_ishift_masked, axes=(0,1))
        img_filtered2 = np.fft.ifft2(back_ishift_masked2, axes=(0,1))
        
        img_back = np.abs(img_back).clip(0,255).astype(np.uint8)
        img_filtered = np.abs(4*img_filtered).clip(0,255).astype(np.uint8)
        img_filtered2 = np.abs(4*img_filtered2).clip(0,255).astype(np.uint8)
        
        img = img_filtered2
        # plt.figure()
        # plt.imshow(img)
        # plt.title('Gray after HP filtered')
        
        # cv2.imshow("ORIGINAL", img)
        # cv2.imshow("SPECTRUM", spec)
        # cv2.imshow("MASK", mask)
        # cv2.imshow("MASK2", mask2)
        # cv2.imshow("ORIGINAL DFT/IFT ROUND TRIP", img_back)
        # cv2.imshow("FILTERED DFT/IFT ROUND TRIP", img_filtered)
        # cv2.imshow("FILTERED2 DFT/IFT ROUND TRIP", img_filtered2)
        
        lower1= [0, 33 , 242 ]
        upper1 = [179, 255, 255]
        
    if mode=='hsv' or mode=='fft_hp':            
                    
        # plt.figure()
        # plt.imshow(img)
        # plt.title('Original')
        hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
        # plt.figure()
        # plt.imshow(hsv)
        # plt.title('HSV colors')
    
        # lower boundary RED color range values
        lower1 = np.array(lower1)
        # upper1 = np.array([20, 255, 255])
        upper1 = np.array(upper1)
        
        # upper boundary RED color range values
        lower2 = np.array(lower2)
        upper2 = np.array(upper2)
        
        lower_mask = cv2.inRange(hsv, lower1, upper1)
        upper_mask = cv2.inRange(hsv, lower2, upper2)
        
        if mask_hsv_low:
            mask = upper_mask+lower_mask
        else:
            mask = lower_mask
        
        # plt.figure()
        # plt.imshow(mask)
        
        res = cv2.bitwise_and(img, img, mask=mask)
        
        final = cv2.cvtColor(res, cv2.COLOR_BGR2RGB)
        
    
        final = rgb2gray(final)
        
        # plt.figure()
        # plt.imshow(final)
        # plt.title('HSV Final')
        
        
        final[final>0.2]=255
        final[final<0.2]=0
        
        # plt.figure()
        # plt.imshow(final)    
        
                
    if mode == 'canny':
        
        
        # img=cv2.imread(path)
        # plt.figure()
        
        # plt.imshow(img)
        # plt.title('Original')
        
        src_gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        img_blur = cv2.blur(src_gray, (3,3))
        threshold1=12
        ratio = 3
        kernel_size=3
        final = cv2.Canny(img_blur, threshold1, 54 ,kernel_size)
        
        # plt.figure()
        # plt.imshow(final)
        # plt.title('Canny Edges')
        

        

        
    
    if angle != 0: #rotate
        (h, w) = final.shape[:2]
        (cX, cY) = (w // 2, h // 2)
        M = cv2.getRotationMatrix2D((cX, cY), angle, 1.0)
        final = cv2.warpAffine(final, M, (w, h)) 
        
        
    # plt.figure()
    # plt.imshow(final)
    # plt.title('After rotation')   
    if mode == 'canny':
        final[final>5]=255
        
    '''Caclulate height of object'''
    y=np.argwhere(final == 255.0 )[:,0]
    x=np.argwhere(final == 255.0 )[:,1]
    
    
    # plt.figure()
    # plt.plot(x,y,'o', color='black')
    # plt.title('Found coordinates')
    # plt.grid()
    
    resolutionCamX=3840 # pixel
    resolutionCamY=1620 # pixel
    lengthChute=5       # m
    widthChute=0.98      # m
    angleLaserLine= 28 # degree
    # calc pixel back to length
    x=x # *lengthChute/resolutionCamX  # m
    y=y #  *widthChute/resolutionCamY   # m
    
    
    
    # calculate average of y values
    x_unique, y_mean = npi.group_by(x).mean(y)
    
    x_std, y_std = npi.group_by(x).std(y)
    
    # plt.figure()
    # plt.plot(x_unique, y_mean)
    # plt.fill_between(x_unique, y_mean-y_std, y_mean+y_std, color = 'lightsteelblue')
    
    # delete outliers which have a negative height
    y_mean=y_mean-offset # get offset out of data 
    # -854 for  # "/home/simon/Documents/TestVideos/2022_05_12_TestswithStrobo/
    # C001C0208_20220512151709_0001/00_ShutterClosed/01_LowerChute/frame1008.jpg"
    
    
    # calc height
    height=y_mean/math.tan(math.radians(angleLaserLine))
    
    # height = savitzky_golay(height, 51, 5)
    
    # plt.figure()
    # # plt.plot(x_unique,y_mean,label='y_mean')
    # plt.plot(x_unique,height,'+',color='black')
    # plt.ylim(-10,100)
    # plt.ylabel('Height / mm')
    # plt.xlabel('Length / mm')
    # # plt.legend()
    # plt.grid()
    
    return x_unique, y_mean, height, y_std

def saveFigure(path, addToName= None): 
        name=path.split('.')
        filename = name[0].split('/')
        folder = np.delete(filename,-1)
        savefig_type = '.jpg'
        seperator = '/'
        folder = seperator.join(folder)
        dd_out = folder +'/00_LaserTriangulation/'
        
        if not os.path.exists(dd_out):
            os.makedirs(dd_out)
        
        if addToName != None:
            dd_out=dd_out+filename[-1] + '_STI'+ addToName + savefig_type
        else:
            dd_out=dd_out+filename[-1] + '_STI'+ savefig_type
        
        plt.savefig(dd_out, dpi=700)
        # plt.close()
        
        print('Saved image to: ',dd_out)
    

if 0: # two lasers 20mW & 100mW
    path = "/home/simon/Documents/TestVideos/2022_05_30_BlueGreenStroboTests/C001C0216_20220530114138_0001/00_ShutterClosed/02_UpperChute/frame1100.jpg"
    # path = '/media/simon/Elements/AvaChute/2022_06_14_FinalTests/C001C0226_20220614095421_0001/00_ShutterClosed/02_UpperChute/frame1110.jpg'
    # [y1, y2, x1, x2]
    area100mWLaser  = [570, 760, 0 , 1560]
    area20mWLaser = [920, 1010, 0 , 1560]
    
    imgLaser100mW = cv2.imread(path)[570:760, 0:1560]
    imgLaser20mW = cv2.imread(path)[920:1010, 0:1560]
    
    plt.figure()
    plt.imshow(imgLaser100mW)

    xL100mw, yL100mW, height_objectsL100mW, y_std100mW = calcLaserTrianguation(imgLaser100mW, angle= 1 , offset = 93, \
                                                                                   save=False, lower1=[0, 45, 50], upper1 = [179, 255, 162])

    xL20mW, yL20mW, height_objectsL20mW , y_std20mW = calcLaserTrianguation(imgLaser20mW, angle= 0.7 , offset = 52, \
                                                                                    save=False, lower1=[0, 45, 50], upper1 = [179, 255, 162])
    '''Figure with fill_between'''
    # plt.figure()
    # plt.plot(xL100mw, height_objectsL100mW, '+', color = 'black', label = r'100 mW Laser')
    # plt.fill_between(xL100mw, height_objectsL100mW-y_std100mW, height_objectsL100mW+y_std100mW, color = 'lightsteelblue', alpha=0.6\
    #                                                                                       , label = r'Standard deviation 100 mW Laser')
    # plt.plot(xL20mW, height_objectsL20mW, '3', color = 'red', label = r'20 mW Laser')
    # plt.fill_between(xL20mW, height_objectsL20mW-y_std20mW, height_objectsL20mW+y_std20mW,\
    #                   edgecolor='#1B2ACC', facecolor='#089FFF',linewidth=1, linestyle='dashdot', antialiased=True ,alpha=0.6,\
    #                   label = r'Standard deviation 20 mW Laser')
    # plt.ylim(-10,40)
    # plt.ylabel(r'Depth / mm')
    # plt.xlabel(r'Length / mm')
    # plt.legend()
    # # plt.title('Comparison of Laser appearance')
    # plt.grid()
    
    '''Figure with errorbar'''
    # plt.figure()
    # plt.errorbar(xL100mw, height_objectsL100mW, xerr = None, yerr = y_std100mW, fmt='kx', ecolor='g', capsize = 2, capthick=2)
    # plt.ylim(-10,40)
    # plt.ylabel(r'Height / mm')
    # plt.xlabel(r'Length / mm')
    # plt.legend()
    # # plt.title('Comparison of Laser appearance')
    # plt.grid()
    
    '''Figure shows std in subplot'''
    fig, axs = plt.subplots(2,1)
    fig.align_ylabels()
    # plt.axis('off')
    # plt.setp(axs, xticks=np.arange(0, shutterClosedGranulate.shape[1], 400), yticks=np.arange(0, shutterClosedGranulate.shape[0], 400))

    yMin = -10
    yMax = 40
    ystep = 10
    # image
    sub1=axs[0].plot(xL100mw, height_objectsL100mW, '+', color = 'black', label = r'100 mW Laser')
    sub1=axs[0].plot(xL20mW, height_objectsL20mW, '3', color = 'red', label = r'20 mW Laser')
    axs[0].set_xlabel(r'$l$ / mm')
    axs[0].set_ylabel(r'$d$ / mm')   
    axs[0].legend()
    axs[0].grid()
    axs[0].set_ylim(yMin,yMax)
    axs[0].set_yticks(np.arange(yMin, yMax+ystep, ystep))
                 
    sub2=axs[1].plot(xL100mw, y_std100mW , '+', color = 'black', label = r'100 mW Laser')
    sub2=axs[1].plot(xL20mW, y_std20mW , '3', color = 'red',  label = r'20 mW Laser')
    axs[1].set_xlabel(r'$l$ / mm')
    axs[1].set_ylabel(r'$std$ / mm')
    axs[1].legend()
    axs[1].grid()
    axs[1].set_ylim(-1,6)
    fig.tight_layout()

    saveFigure(path)

if 0: # show height of reference objects
    
    path = "/home/simon/Documents/TestVideos/2022_05_12_TestswithStrobo/C001C0208_20220512151709_0001/00_ShutterClosed/01_LowerChute/frame1008.jpg"
    
    imgTestObjects = cv2.imread(path)[800:960,:]
    # plt.figure()
    # plt.imshow(imgTestObjects)
    xRefObj, yRefObj, height_refobjects, y_stdRefObj = calcLaserTrianguation(imgTestObjects, angle= 1.20 , offset = 54, \
                                                                                   save=False, lower1=[0, 45, 50], upper1 = [179, 255, 162],\
                                                                                       mode = 'fft_hp')
    '''Figure with fill_between'''
    plt.figure()
    plt.plot(xRefObj, height_refobjects, '+', color = 'black')# , label = r'Height Reference Objects')
    # plt.fill_between(xRefObj, height_refobjects-y_stdRefObj, height_refobjects+y_stdRefObj, color = 'lightsteelblue', alpha=0.6\
    #                                                                                       , label = r'Standard deviation of Height')
    plt.ylim(-10, 110)
    plt.xlabel('Length / mm')
    plt.ylabel('Height / mm')
    # plt.legend()
    plt.grid()
    
    '''Figure with standard deviation columns'''
    # plt.figure()
    # plt.errorbar(xRefObj, height_refobjects, xerr = None, yerr = y_stdRefObj, label = r'Height Reference Objects', \
    #              fmt='kx', ecolor='g', capsize = 2, capthick=2)
    # plt.legend()
    # plt.grid()
    # plt.ylim(-10, 60)
    # plt.xlabel('Length / mm')
    # plt.ylabel('Height / mm')
    
    saveFigure(path)
    
if 1: # compare all techniques
    path = "/home/simon/Documents/TestVideos/2022_05_12_TestswithStrobo/C001C0208_20220512151709_0001/00_ShutterClosed/01_LowerChute/frame1008.jpg"
    
    imgTestObjects = cv2.imread(path)[800:960,:]
    imgTestObjects_wholeImage = cv2.imread(path)
    # plt.figure()
    # plt.imshow(imgTestObjects)
    xRefObj_fft_hp, yRefObj_fft_hp, height_refobjects_fft_hp, y_stdRefObj_fft_hp = calcLaserTrianguation(imgTestObjects, angle= 1.20 , offset = 54, \
                                                                                   save=False, lower1=[0, 45, 50], upper1 = [179, 255, 162],\
                                                                                       mode = 'fft_hp')
    xRefObj_hsv, yRefObj_hsv, height_refobjects_hsv, y_stdRefObj_hsv = calcLaserTrianguation(imgTestObjects, angle= 1.20 , offset = 54, \
                                                                                   save=False, lower1=[0, 45, 50], upper1 = [179, 255, 162],\
                                                                                       mode = 'hsv')
    xRefObj_canny, yRefObj_canny, height_refobjects_canny, y_stdRefObj_canny = calcLaserTrianguation(imgTestObjects, angle= 1.20 , offset = 54, \
                                                                                       save=False, lower1=[0, 45, 50], upper1 = [179, 255, 162],\
                                                                                           mode = 'canny')
    xRefObj_gray, yRefObj_gray, height_refobjects_gray, y_stdRefObj_gray = calcLaserTrianguation(imgTestObjects, angle= 1.20 , offset = 54, \
                                                                                       save=False, lower1=[0, 45, 50], upper1 = [179, 255, 162],\
                                                                                           mode = 'grayThresh')
        
    '''Figure shows std in subplot'''
    fig, axs = plt.subplots(2,2)
    fig.align_ylabels()
    # plt.axis('off')
    # plt.setp(axs, xticks=np.arange(0, shutterClosedGranulate.shape[1], 400), yticks=np.arange(0, shutterClosedGranulate.shape[0], 400))

    # image
    # sub1=axs[0,0].imshow(imgTestObjects_wholeImage)
    # axs[0,0].set_xlabel(r'Length / mm')
    # axs[0,0].set_ylabel(r'Height / mm')
    # axs[0,0].legend()
    
    minY = -10
    maxY = 100

    axs[0, 0].set_title(r'a) Gray Threshold')
    axs[1, 0].set_title(r'c) HP filter \& HSV Threshold')
    axs[0, 1].set_title(r'b) HSV Threshold')
    axs[1, 1].set_title(r'd) Canny Edge Detection')
    
    
    sub1=axs[0,0].plot(xRefObj_gray, height_refobjects_gray, '+', color = 'black')#, label = r'Gray Threshold')
    axs[0,0].set_xlabel(r'$l$ / mm')
    axs[0,0].set_ylabel(r'$d$ / mm')
    axs[0,0].grid()
    #axs[0,0].legend()
    axs[0,0].set_ylim(minY,maxY)
                 
    sub2=axs[1,0].plot(xRefObj_fft_hp, height_refobjects_fft_hp,'+', color = 'black')#, label = r'FFT_HP')
    axs[1,0].set_xlabel(r'$l$ / mm')
    axs[1,0].set_ylabel(r'$d$ / mm')
    #axs[1,0].legend()
    axs[1,0].grid()
    axs[1,0].set_ylim(minY,maxY)
    
    sub2=axs[0,1].plot(xRefObj_hsv, height_refobjects_hsv ,'+', color = 'black')#, label = r'HSV')
    axs[0,1].set_xlabel(r'$l$ / mm')
    axs[0,1].set_ylabel(r'$d$ / mm')
    #axs[0,1].legend()
    axs[0,1].grid()
    axs[0,1].set_ylim(minY,maxY)
    
    sub2=axs[1,1].plot(xRefObj_canny, height_refobjects_canny ,'+', color = 'black')#, label = r'Canny')
    axs[1,1].set_xlabel(r'$l$ / mm')
    axs[1,1].set_ylabel(r'$d$ / mm')
    #axs[1,1].legend()
    axs[1,1].grid()
    axs[1,1].set_ylim(minY,maxY)
    
    ylimMin = -10
    ylimMax = 100
    ysteps  = 20
    
    xlimMax = 1800
    
    xsteps = 300
    
    for a in axs.flat:
        a.set_xlim(0, 1800)
        a.set_yticks(np.arange(0,ylimMax+ysteps,step = ysteps))
        a.set_xticks(np.arange(0,xlimMax+xsteps,step = xsteps))
        a.label_outer()

    fig.tight_layout()
    
    saveFigure(path, 'compMethods')    
    
if 0: # find height of point over time and compare with point laser
    folder='/media/simon/radar_server/AvaChute/2022_07_01_NoLightLaserEvaluation/C001C0231_20220701104827_0001/00_ShutterClosed/02_UpperChute/'
    path = folder+"*.jpg"
    
    fns = sorted(glob.glob(path))

    t_vec = []
    depth = []
    T = 1/160
    
    for i in fns:
        imgn = cv2.imread(i)[620:653,172:173]
        x, _, height, y_std = calcLaserTrianguation(imgn,angle= None\
                                    , offset = 15, save=False, lower1=[0, 45, 50], upper1 = [179, 255, 162], mode = 'hsv')
        
        name=i.split('.')
        filename = name[0].split('/')
        t = round((int(filename[-1].replace('frame',''))-1000) * T, 5)
        
        if height.size > 0:
            t_vec.append(t)
            depth.append(height[0])
            
    # load point laser measurements
    nameTxtFile = '/home/simon/Documents/AvalancheChute/00_DAQ/data/Rutsche-2022-06-14-09-47-33.h5'
    data, daq_settings = DataEval.load_DAQ_data(nameTxtFile)
    
    angle_Balluff = 15
    
    baluff_1 = ((data[:,6]/0.02)-77.5)*(-1)*10*abs(math.cos(angle_Balluff))
    baluff_2 = ((data[:,7]/0.02)-77.5)*(-1)*10*abs(math.cos(angle_Balluff))
    
    window_size = 500
    
    arr = baluff_2
    t_average= []
    
    sampling_rate = 20e3
    timeStamp=1/sampling_rate
    time = np.arange(0,timeStamp*len(data),timeStamp) 
  
    i = 0
    # Initialize an empty list to store moving averages
    moving_averages = []
      
    # Loop through the array to consider
    # every window of size 3
    while i < len(arr) - window_size + 1:
        
        # Store elements from i to i+window_size
        # in list to get the current window
        window = arr[i : i + window_size]
      
        # Calculate the average of current window
        window_average = round(sum(window) / window_size, 2)
          
        # Store the average of current
        # window in moving average list
        moving_averages.append(window_average)
        t_average.append(time[i+250])
        # Shift window to right by one position
        i += 1
        
    markersize = 2
        
    plt.figure(figsize = nf.set_size(416,unit='pt'))
    plt.plot(time, baluff_2, '-',linewidth = 0.3, color='gray', label = 'Point Laser')
    plt.plot(t_average, moving_averages, '--', label = 'Point Laser Averaged')
    plt.plot(t_vec, depth, 'o', markersize = markersize, c='k', label = 'Structured light illumination')
    plt.xlim(0 , 8)
    plt.ylim(-40, 40)
    plt.xlabel('$t$ / s')
    plt.ylabel('$d$ / mm')
    plt.legend()
    plt.grid()
    plt.tight_layout()
    
    dd_out = folder + 'CompBalluff_LineLaser/Balluff_LineLaser100mW.jpg'
    plt.savefig(dd_out, dpi=300)
    
    
    
    



