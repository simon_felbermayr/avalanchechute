#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar  8 11:10:22 2022

@author: simon
"""
import matplotlib.pyplot as plt

f_sample_6=5.5e3 # Hz
f_sample_10=10e3 # Hz
f_sample_20=20e3 # Hz
maxVel=[2, 4, 8, 10] # m/s
s_6= [element * 1/f_sample_6*10e3 for element in maxVel] # mm
s_10= [element * 1/f_sample_10*10e3 for element in maxVel] # mm
s_20= [element * 1/f_sample_20*10e3 for element in maxVel] # mm

plt.plot(maxVel,s_6,label="f_sample=5.5 kHz")
plt.plot(maxVel,s_10,label="f_sample=10 kHz")
plt.plot(maxVel,s_20,label="f_sample=20 kHz")
plt.ylabel('Resolution / mm')
plt.xlabel('Velocity / m/s')
plt.legend(loc="upper left")
plt.grid()
plt.show()
