# -*- coding: utf-8 -*-
"""
Created on Mon Feb 21 10:04:04 2022

@author: Felbermayr Simon
"""
from openpiv import tools, pyprocess, validation, filters, scaling 
import numpy as np
import matplotlib.pyplot as plt
#%matplotlib inline
import imageio
import cv2 
import os 

# create frames out of video

def vid2frame(args):
    # function to cut frames out of video
    # args contains path were video can be found and in which folder the
    # frames should be saved
    path, saveDirect =args
    video = cv2.VideoCapture(path) 
    try:  
    	if not os.path.exists(saveDirect): 
    		os.makedirs(saveDirect) 
    except OSError: 
    	print ('Error: Folder cant be found or created') 
    currentframe = 0
    while(True): 
    	ret,frame = video.read() 
    
    	if ret: 
    		name = './'+saveDirect+'/frame' + str(currentframe) + '.jpg'
    		print ('Captured...' + name) 
    		cv2.imwrite(name, frame) 
    		currentframe += 1
    	else: 
    		break
    video.release() 
    cv2.destroyAllWindows()
    



path=r'./PIV.MTS'
saveDirect='frames'
args=path,saveDirect
vid2frame(args)