# -*- coding: utf-8 -*-
"""
Created on Tue May  3 15:59:59 2022

@author: Felbermayr Simon
"""

import h5py
import glob
import cv2
import matplotlib.pyplot as plt
from scipy.ndimage import zoom
from multiprocessing import Pool
import os
import skimage.color
import skimage.io
import numpy as np

plt.close('all')

# filename='CameraCalibration.h5'

# data = h5py.File(filename, 'r')
# for group in data.keys() :
#     print (group)
#     for dset in data[group].keys():      
#         print (dset)

def clipped_zoom(img, zoom_dim, **kwargs):
    # function completly from 
    # https://stackoverflow.com/questions/37119071/scipy-rotate-and-zoom-an-image-without-changing-its-dimensions

    top,  topzh, left, leftzw, h, w, zoom_factor = zoom_dim
    
    zoom_tuple = (zoom_factor,) * 2 + (1,) * (img.ndim - 2)
    
    out = zoom(img[int(top):int(topzh), int(left):int(leftzw)], zoom_tuple, **kwargs)
    
    # `out` might still be slightly larger than `img` due to rounding, so
    # trim off any extra pixels at the edges
    trim_top = ((out.shape[0] - h) // 2)
    trim_left = ((out.shape[1] - w) // 2)
    out = out[int(trim_top):int(trim_top+h), int(trim_left):int(trim_left+w)]
    
    return out
    
def checkRollingShutterClosed(img):
    
    name= img.split('.')
    filename=name[0].split('/')
        
    frame=cv2.imread(img)#[:,0:1850]
    
    if frame is not None:
        
        checkShutterImage = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

        # check some edge pixels to determine if shutter is closed
        checkIfShutterClosed=[]
        
        # two points only for green-black-blue strobo
        checkIfShutterClosed.append(checkShutterImage[102,172])
        checkIfShutterClosed.append(checkShutterImage[102, 1789])
        checkIfShutterClosed.append(checkShutterImage[1580, 530])
        checkIfShutterClosed.append(checkShutterImage[1549, 1839])
        
        if all(img <= 48 for img in checkIfShutterClosed):
            folder_fully_closed = folder+'00_FullyClosed/'
           
            if not os.path.exists(folder_fully_closed):
                 os.makedirs(folder_fully_closed)

            cv2.imwrite(folder_fully_closed+filename[-1]+'.jpg',frame)    

            
            
    
def calibrateImagesFromFolder(img):
    # frames=sorted(glob.glob(folder+"*.jpg")+glob.glob(folder+"*.JPG"))
    # # print(frames)
    # j=0
    # print('Seperation of Chute part started')
    # for img in frames:
        # print(img)
        
    name= img.split('.')
    filename=name[0].split('/')
        
    frame=cv2.imread(img)#[:,0:1850]
    # plt.figure()
    # plt.imshow(frame)
    
    if frame is not None:
        
        checkShutterImage = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

        # check some edge pixels to determine if shutter is closed
        checkIfShutterClosed=[]
        # checkIfShutterClosed.append(checkShutterImage[75,96])
        # checkIfShutterClosed.append(checkShutterImage[50, 2421])
        # checkIfShutterClosed.append(checkShutterImage[1519, 3775])
        # checkIfShutterClosed.append(checkShutterImage[1500, 83])
        
        # two points only for green-black-blue strobo
        checkIfShutterClosed.append(checkShutterImage[1220,1143])
        checkIfShutterClosed.append(checkShutterImage[40, 1850])
        # checkIfShutterClosed.append(checkShutterImage[500, 3400])
        # checkIfShutterClosed.append(checkShutterImage[1200, 3400])

        # if all(img >= 50 for img in checkIfShutterClosed):
        # print('Shutter is opened')
        # 1850 ist knick der Rutsche (Änderung der Steigung)
        ChutePartLow = frame[:,0:2000]
        ChutePartUp = frame[:,1700:]
        
        # print(mtx_low)
        # dst = cv2.undistort(ChutePartLow, mtx_low, dist_low, None, newcameramtx_low)
        dst = cv2.warpPerspective(ChutePartLow,Mperspective_low,(threshLength_low[0],threshWidth_low[0]))
        dst = cv2.undistort(dst, mtxFine_low, distFine_low, None, newcameramtxFine_low)
        dst=clipped_zoom(dst,zoomfactor_low)
        
        # plt.figure()
        # plt.imshow(dst)
        # plt.axhline(y=1200, color='r', linestyle='-')
        
        # print(dst.shape)
        
        # dstUP = cv2.undistort(ChutePartUp, mtx_up, dist_up, None, newcameramtx_up)
        dstUP = cv2.warpPerspective(ChutePartUp,Mperspective_up,(threshLength_up[0],threshWidth_up[0]))
        dstUP = cv2.undistort(dstUP, mtxFine_up, distFine_up, None, newcameramtxFine_up)
        dstUP=clipped_zoom(dstUP,zoomfactor_up)
        # print(dstUP.shape)
        
        '''show overall frame'''
        # frame = cv2.hconcat([dst, dstUP])
        
        # plt.figure()
        # plt.imshow(frame)
        # plt.axhline(y=400, color='r', linestyle='-')
        # plt.axhline(y=475, color='r', linestyle='-')
        # plt.axhline(y=1200, color='r', linestyle='-')
        if all(img >= 48 for img in checkIfShutterClosed):
            
            folder_up = folder+'02_UpperChute/'
            folder_low = folder+'01_LowerChute/'
            
            if not os.path.exists(folder_up):
                os.makedirs(folder_up)
                
            if not os.path.exists(folder_low):
                os.makedirs(folder_low)
            
            
            cv2.imwrite(folder_up+filename[-1]+'.jpg',dstUP[:,0:1930])
            cv2.imwrite(folder_low+filename[-1]+'.jpg',dst[:,0:1840])
            print('Shutter opened')
        else:
            
            folder_up = folder+'00_ShutterClosed/'+'02_UpperChute/'
            folder_low = folder+'00_ShutterClosed/'+'01_LowerChute/'
            
            if not os.path.exists(folder_up):
                os.makedirs(folder_up)
                
            if not os.path.exists(folder_low):
                os.makedirs(folder_low)
                
            cv2.imwrite(folder_up+filename[-1]+'.jpg',dstUP[:,0:1930])
            cv2.imwrite(folder_low+filename[-1]+'.jpg',dst[:,0:1840])
            print('Shutter closed')
        # else:
        #     print('Shutter is closed')
        #     cv2.imwrite(folder+'00_ShutterClosed/'+filename[-1]+'.jpg',frame)
    else:
        print('Frame is empty!')
    # print('Seperation of Chute part finished')


frame=cv2.imread('/media/simon/Elements/AvaChute/2022_06_14_FinalTests/C001C0226_20220614095421_0001/00_ShutterClosed/02_UpperChute/frame1396.jpg')
checkShutterImage = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

plt.figure()
plt.imshow(frame)
# plt.plot(1143,1220,'ro')
# plt.plot(1850,40,'ro')


# histogram, bin_edges = np.histogram(frame)#, bins=256, range=(0, 1))
# # configure and draw the histogram figure
# plt.figure()
# plt.title("Grayscale Histogram")
# plt.xlabel("grayscale value")
# plt.ylabel("pixel count")
# # plt.xlim([0.0, 100.0])  # <- named arguments do not work here

# plt.plot(bin_edges[0:-1], histogram)  # <- or here
# plt.show()



    

filename='CameraCalibration.h5'
data = h5py.File(filename, 'r')

# upper part
mtx_up  = data['UpperChute']['CamMatrix']['noPerspective'][:]
dist_up = data['UpperChute']['Distortion']['noPerspective'][:]
newcameramtx_up = data['UpperChute']['RefinedCamMatrix']['noPerspective'][:]

Mperspective_up = data['UpperChute']['ChangePerspective']['Matrix'][:]
threshLength_up = data['UpperChute']['ChangePerspective']['ThresholdLength'][:]
threshWidth_up = data['UpperChute']['ChangePerspective']['ThresholdWidth'][:]

mtxFine_up  = data['UpperChute']['CamMatrix']['newPerspective'][:]
distFine_up = data['UpperChute']['Distortion']['newPerspective'][:]
newcameramtxFine_up = data['UpperChute']['RefinedCamMatrix']['newPerspective'][:]

zoomfactor_up = data['UpperChute']['Zoom']['Area[X,Y]'][:]

# lower part
mtx_low  = data['LowerChute']['CamMatrix']['noPerspective'][:]
dist_low = data['LowerChute']['Distortion']['noPerspective'][:]
newcameramtx_low= data['LowerChute']['RefinedCamMatrix']['noPerspective'][:]

Mperspective_low = data['LowerChute']['ChangePerspective']['Matrix'][:]
threshLength_low = data['LowerChute']['ChangePerspective']['ThresholdLength'][:]
threshWidth_low = data['LowerChute']['ChangePerspective']['ThresholdWidth'][:]

mtxFine_low  = data['LowerChute']['CamMatrix']['newPerspective'][:]
distFine_low = data['LowerChute']['Distortion']['newPerspective'][:]
newcameramtxFine_low = data['LowerChute']['RefinedCamMatrix']['newPerspective'][:]

zoomfactor_low = data['LowerChute']['Zoom']['Area[X,Y]'][:]

# calibrateImagesFromFolder('/home/simon/Documents/TestVideos/2022_05_12_TestswithStrobo/C001C0207_20220512150814_0001/')
# calibrateImagesFromFolder('/home/simon/Documents/TestVideos/2022_04_22_Tests_AllSensorsCam/C001C0177_20220422160244_0001/data-raw/')

folder = '/media/simon/Elements/AvaChute/2022_06_14_FinalTests/C001C0227_20220614100145_0001/'

frames=sorted(glob.glob(folder+"*.jpg")+glob.glob(folder+"*.JPG"))

# process data quasiparallel
with Pool(10) as p:
        p.map(calibrateImagesFromFolder, frames)


if 0: # check if rolling shutter is partly opened or fully
    folder = '/media/simon/Elements/AvaChute/2022_06_14_FinalTests/C001C0226_20220614095421_0001/00_ShutterClosed/02_UpperChute/'
    
    frames=sorted(glob.glob(folder+"*.jpg")+glob.glob(folder+"*.JPG"))
    
    # process data quasiparallel
    with Pool(10) as p:
            p.map(checkRollingShutterClosed, frames)

    


