import h5py

def write_hdf5(fnout: str, group_name: str,group_name2: str, frame_name: str, vel_matrix):
    # save raw data to hdf5
    # fnout = filename
    # group_name ist ein string, z.b. "v_tot" oder "v_horz"
    # frame_name ist ein string, z.b. "1024" oder "123"

    #if os.path.isfile(fnout):
    #    os.remove(fnout)

    with h5py.File(fnout, 'a') as h5f:
        
        if '/'+group_name not in h5f.keys():
            # if abfrage wahrscheinlich nicht korrekter code... 
            # einfach halt testen ob group schon vorhanden im hdf5 file

            # ansonsten make group
            grp = h5f.create_group(group_name)
            
            if '/'+group_name+'/'+group_name2 not in h5f.keys() & group_name2!=None:
                grp2 = grp.create_group(group_name2)

        
        if group_name2=='None' :
            # save array in dataset
            h5f.create_dataset("/"+group_name+"/"+frame_name, data=vel_matrix)
        else:
            h5f.create_dataset("/"+group_name+"/"+group_name2+"/"+frame_name, data=vel_matrix)
        
            
        h5f.flush()
        
    pass
            
# Low-level functions
def hdf5_load_dict(filename, dict_name):
    f = h5py.File(filename, 'r')
    param_grp = f[dict_name]
    dictionary = {}
    for key in param_grp.keys():
        if isinstance(param_grp[key], h5py._hl.group.Group):
            # if there is a subgroup, recursively load to dict
            dictionary[key] = hdf5_load_dict(filename, dict_name+'/'+key)
        else:
            dictionary[key] = param_grp[key][()]
            if isinstance(dictionary[key], bytes):
                # assume, the new version of h5py read string as bytes
                dictionary[key] = dictionary[key].decode("utf-8")
    return dictionary

def hdf5_write_dict(filename, dict_name, dictionary):
    #print(filename)
    f = h5py.File(filename, 'a')
    param_grp = f.create_group(dict_name)
    # write single item
    for key, item in dictionary.items():
        try:
            if item is None:
                item = 'None'
            if isinstance(item, dict):
                # recursive write each dictionary
                hdf5_write_dict(filename, dict_name+'/'+key, item)
            else:
                f.create_dataset("/"+dict_name+"/{}".format(key), data=item)
        except:
            print("failed parameter write to hdf5:", key, "value", item)
    return
