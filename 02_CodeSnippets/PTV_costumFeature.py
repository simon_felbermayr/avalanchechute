#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr 14 09:20:11 2022

@author: simon
"""

import numpy as np
import pandas as pd

import pims
import trackpy as tp
import os
import cv2 

import matplotlib  as mpl 
import matplotlib.pyplot as plt 

# Optionally, tweak styles.
mpl.rc('figure',  figsize=(10, 6))
mpl.rc('image', cmap='gray')


import skimage
import matplotlib.patches as mpatches
from matplotlib.pyplot import quiver


# folder="../01_PIV/frames_B005C0128_20220413143345_0001/"
# folder="../01_PIV/frames_B005C0103_20220407150600_0001/"
folder = "/home/simon/Documents/TestVideos/2022_04_22_Tests_AllSensorsCam/C001C0183_20220422162517_0001/bestof/"

# crop_area=[730:1213,2422:3158]

binary=False
chuteBallExp=False
id_example=4
fps=160
lengthChute=5 #m
lenseRes=3840 #pixel
micronsperpixel=lengthChute/lenseRes


frames = []


for filename in sorted(os.listdir(folder)):
    if chuteBallExp:
        img = cv2.imread(os.path.join(folder,filename))[418:1226,1867:3726]
    
        if img is not None:
            # specific for orange ball to track
            reddishch0 = img[:, :, 0]<170
            reddishch1 = img[:, :, 1]<170
            reddishch2 = img[:, :, 2]>170
            reddish=reddishch0*reddishch1*reddishch2
            img[reddish] = [0, 0, 1]
            img[reddish] = [0, 0, 2]
            img[reddish] = [0, 0, 3]
            	
            image = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)   

            frames.append(img)
            
            
    else: 
        img = cv2.imread(os.path.join(folder,filename),cv2.IMREAD_GRAYSCALE)
    
        if img is not None:
            if binary:
                # turn bw
                (thresh, im_bw) = cv2.threshold(img, 100, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)
            else:
                im_bw=img
            # crop image
            frames.append(im_bw[418:1226,1867:3726])#
# window for segment 2 of chute [418:1226,1867:3726]       
# window for coin experiment [94:1498,1673:2600])

plt.imshow(frames[1])
img_example=frames[id_example]

# t1 = tp.filter_stubs(t, 25)

features = pd.DataFrame()
for num, img in enumerate(frames):
    white = 255
    label_image = skimage.measure.label(img, background=white)
    for region in skimage.measure.regionprops(label_image, intensity_image=img):
        # Everywhere, skip small and large areas
        # area size for coin <200 >500
        if region.area < 1 or region.area > 100:
            continue
        # Only black areas
        if region.mean_intensity > 1:
            continue
        # # On the top, skip small area with a second threshold
        # if region.centroid[0] < 260 and region.area < 80: 
        #     continue
        # Store features which survived to the criterions
        try:
            features = features.append([{'y': region.centroid[0],
                                         'x': region.centroid[1],
                                         'frame': num,
                                         },])
        except:
            print("No features found!")

plt.figure()
tp.annotate(features[features.frame==(id_example+1)], frames[44]);



search_range = 500
t = tp.link_df(features, search_range, memory=20)

# for i in range(0,len(t['y'].values)-1):
#     if i>0:
#         if (t['y'].values[i-1]>t['y'].values[i]):
#             t['y'].values[i-1]=t['y'].values[i]


tp.plot_traj(t, superimpose=frames[len(frames)-1])


unstacked = t.set_index(['frame', 'particle']).unstack()

data = pd.DataFrame()
for item in set(t.particle):
    sub = t[t.particle==item]
    dvx = np.diff(sub.x)
    dvy = np.diff(sub.y)
    for x, y, dx, dy, frame in zip(sub.x[:-1], sub.y[:-1], dvx, dvy, sub.frame[:-1],):
        v_x=dx*micronsperpixel/(1/fps)
        v_y=dy*micronsperpixel/(1/fps)
        data = data.append([{'dx': dx, 
                              'dy': dy,
                              'v_x':v_x,
                              'v_y':v_y,
                              'x': x,
                              'y': y,
                              'frame': frame,
                              'particle': item,
                            }])
        
i = 10
rawframes=frames
d = data[data.frame==i]
plt.imshow(rawframes[i])
plt.quiver(d.x, d.y, d.dx, -d.dy, pivot='middle', headwidth=4, headlength=6, color='red')
plt.axis('off')

plt.figure()
plt.imshow(frames[44])
