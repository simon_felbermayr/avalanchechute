#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May 26 17:11:21 2022

@author: simon
"""
from multiprocessing import Pool
import os
import numpy as np
import cv2 as cv
import glob
import matplotlib.pyplot as plt
from matplotlib import rc

plt.close('all')

plt.rcParams['text.usetex'] = True

def getTimestamp(pathImg):
    
    name=pathImg.split('.')
    filename = name[0].split('/')
    
    # calc timestamp
    dt = int(filename[-1].replace('frame',''))
    
    # frames per second ZCam
    fps = 160
    T = 1/fps
    
    timestamp=str(round((dt-1500)*T*1000,5)) # -1000 because save names; *1000 to have ms
    
    return timestamp
if 1: # plot bright and dark phase
    # frames to plot
    shutterClosedBoxes = cv.imread("/home/simon/Documents/TestVideos/2022_05_12_TestswithStrobo/C001C0208_20220512151709_0001/00_ShutterClosed/01_LowerChute/frame1008.jpg")
    shutterOpenBoxes = cv.imread("/home/simon/Documents/TestVideos/2022_05_12_TestswithStrobo/C001C0208_20220512151709_0001/01_LowerChute/frame1000.jpg" )
    
    shutterClosedGranulate = cv.imread("/home/simon/Documents/TestVideos/2022_05_12_TestswithStrobo/C001C0207_20220512150814_0001/00_ShutterClosed/01_LowerChute/frame1160.jpg")
    shutterOpenGranulate = cv.imread("/home/simon/Documents/TestVideos/2022_05_12_TestswithStrobo/C001C0207_20220512150814_0001/01_LowerChute/frame1158.jpg")
    
    shutterClosedBoxes = cv.cvtColor(shutterClosedBoxes,cv.COLOR_BGR2RGB)
    shutterOpenBoxes = cv.cvtColor(shutterOpenBoxes,cv.COLOR_BGR2RGB)
    shutterClosedGranulate = cv.cvtColor(shutterClosedGranulate,cv.COLOR_BGR2RGB)
    shutterOpenGranulate = cv.cvtColor(shutterOpenGranulate,cv.COLOR_BGR2RGB)
    
    #plt.figure()
    fig, axs = plt.subplots(2, 2)
    # plt.axis('off')
    plt.setp(axs, xticks=np.arange(0, shutterClosedBoxes.shape[1], 400), yticks=np.arange(0, shutterClosedBoxes.shape[0], 400))
    
    # fig.suptitle(r'Timestamp: '+timestamp+' ms', fontsize=16)
    axs[0, 0].set_title(r'a) Bright phase with Ref Objects')
    axs[0, 1].set_title(r'b) Dark phase with Ref Objects')
    axs[1, 0].set_title(r'c) Bright phase with Granulate')
    axs[1, 1].set_title(r'd) Dark phase with Granulate')
    
    # image
    sub1=axs[0,0].imshow(shutterOpenBoxes)
    axs[0,0].set_xlabel(r'$l$ / mm')
    axs[0,0].set_ylabel(r'$w$ / mm')
    # vx component
    sub2=axs[0,1].imshow(shutterClosedBoxes)
    axs[0,1].set_xlabel(r'$l$ / mm')
    axs[0,1].set_ylabel(r'$w$ / mm')
    # vy component
    sub3=axs[1,0].imshow(shutterOpenGranulate)
    axs[1,0].set_xlabel(r'$l$ / mm')
    axs[1,0].set_ylabel(r'$w$ / mm')
    # v abs
    sub4=axs[1,1].imshow(shutterClosedGranulate)
    axs[1,1].set_xlabel(r'$l$ / mm')
    axs[1,1].set_ylabel(r'$w$ / mm')
    
    # set plot settings
    plt.subplots_adjust(left=None, bottom=None, right=None, top=None, wspace=0.3, hspace=0.5)
    
    dd_out = '/home/simon/Documents/TestVideos/2022_05_12_TestswithStrobo/ShowLaserAppearance.jpg'
    
    plt.savefig(dd_out, dpi=700)
    #plt.close()

###############################################################################################################
''' Plot Laser Appearance for 20mW and 100mW '''
###############################################################################################################

if 0: #plot laser appearance 20mW100mW
    # frames to plot
    
    path1 = "/home/simon/Documents/TestVideos/2022_05_13_TestwithStroboNewLaserPos/C001C0212_20220513162228_0001/00_ShutterClosed/02_UpperChute/frame1629.jpg"
    path2 = "/home/simon/Documents/TestVideos/2022_05_13_TestwithStroboNewLaserPos/C001C0212_20220513162228_0001/02_UpperChute/frame1621.jpg"
    path3 = "/home/simon/Documents/TestVideos/2022_05_13_TestwithStroboNewLaserPos/C001C0212_20220513162228_0001/02_UpperChute/frame1633.jpg"
    
    shutterClosedGranulate = cv.imread(path1)
    shutterOpenGreenGranulate = cv.imread(path2)
    shutterOpenBlueGranulate = cv.imread(path3)
    
    
    shutterClosedGranulate = cv.cvtColor(shutterClosedGranulate,cv.COLOR_BGR2RGB)
    shutterOpenGreenGranulate = cv.cvtColor(shutterOpenGreenGranulate,cv.COLOR_BGR2RGB)
    shutterOpenBlueGranulate = cv.cvtColor(shutterOpenBlueGranulate,cv.COLOR_BGR2RGB)
    
    #plt.figure()
    fig, axs = plt.subplots(3,1)
    # plt.axis('off')
    plt.setp(axs, xticks=np.arange(0, shutterClosedGranulate.shape[1], 400), yticks=np.arange(0, shutterClosedGranulate.shape[0], 400))
    
    timestampDark = getTimestamp(path1)
    timestampGreen = getTimestamp(path2)
    timestampBlue = getTimestamp(path3)
        
    
    # fig.suptitle(r'Timestamp: '+timestamp+' ms', fontsize=16)
    axs[0].set_title(r'Timestamp '+ timestampGreen)
    axs[1].set_title(r'Timestamp '+ timestampDark)
    axs[2].set_title(r'Timestamp '+ timestampBlue)
    
    # image
    sub1=axs[0].imshow(shutterOpenGreenGranulate)
    axs[0].set_xlabel(r'Length / mm')
    axs[0].set_ylabel(r'Width / mm')
    # vx component
    sub2=axs[1].imshow(shutterClosedGranulate)
    axs[1].set_xlabel(r'Length / mm')
    axs[1].set_ylabel(r'Width / mm')
    # vy component
    sub3=axs[2].imshow(shutterOpenBlueGranulate)
    axs[2].set_xlabel(r'Length / mm')
    axs[2].set_ylabel(r'Width / mm')
    
    # set plot settings
    plt.subplots_adjust(left=None, bottom=None, right=None, top=None, wspace=0.5, hspace=0.5)
    
    # dd_out = '/home/simon/Documents/TestVideos/2022_05_12_TestswithStrobo/ShowLaserAppearance.jpg'
    
    # plt.savefig(dd_out, dpi=700)
    # plt.close()
