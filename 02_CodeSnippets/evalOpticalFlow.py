import h5py
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import cv2
import nice_figures as nf

plt.rcParams['text.usetex'] = True
plt.rcParams['font.size'] = 12

# filename='C001C0177_20220422160244_0001.h5'
# filename = 'CameraCalibration.h5'

def readOFHDF5(filename, path, poi, keeptimestamp = True, saveFile = False):

    
    frame=cv2.imread(path)
    plt.figure()
    plt.imshow(frame)
    plt.plot(poi[0],poi[1],'ro')
    plt.xlabel(r'Length / mm')
    plt.ylabel(r'Width / mm')


    name=filename.split('.')
    name=np.delete(name[0].split('/'),-1)
    save_velProfileTo='VelocityProfile_'+name[-1]+name[-3]+str(poi[0])+'_'+str(poi[1])+'.jpg'
    dd_out="/".join(np.append(name,save_velProfileTo))

    vel_mtx=[]


    i=0
    j=0
    prev_t=0
    fps=160
    T=1/fps
      
    n = 1000
    vel_mtx = np.zeros((n,4)) # Pre-allocate matrix
    time = np.zeros((n,1)) # Pre-allocate matrix
    
    data = h5py.File(filename, 'r')
    for group in data.keys() :
        print("Chutepart:", group)
        for dset in data[group].keys():      
            print("  Group:", dset)
            if dset!='None' and group!='parameter':
                if group=='timestamp':
                    
                    t=data[group][dset][:][0]
                    time[j]=prev_t+t
                    prev_t=time[j]
                    j+=1
                else:
                    vel_dyn = data[group][dset][poi[1],poi[0]]
                    vel_mtx[j,i]=vel_dyn
                    j+=1
                        
        if group!='parameter':
            j=0
            i+=1
    
    vel=vel_mtx[:np.where(vel_mtx[:,1:]==0)[0][0],:] # get m/s 
    dt=time[:np.where(time[:,:]==0)[0][0],:]
    
    # find timestamp where jump in time is happening
    delete_index=[]
    
    for i in range(1,len(dt)): 
        if round((time[i]-time[i-1])[0],5)>T:
            delete_index=np.append(delete_index,[i],axis=0)
    
            print('Element deleted: ',i)
            
    if delete_index != []:
        delete_index=delete_index.astype(int)
        
    if keeptimestamp:
        # show all velocities, even with bigger timestamp than 0.00625 s
        delete_index = []
        
    dt_filtered = np.delete(dt, delete_index)
    vel_abs = np.delete(vel[:,1], delete_index)
    vel_vx = np.delete(vel[:,2], delete_index)
    vel_vy = np.delete(vel[:,3], delete_index)
    
    xlimMax = 3.5
    xlimMin = 0
    xsteps = 0.25
    
    ylimMax = 4
    ylimMin = -4
    ysteps = 1
    if saveFile:
        plt.figure(figsize = nf.set_size(416,unit='pt'))
        plt.xticks(np.arange(xlimMin, xlimMax+xsteps, xsteps))
        plt.yticks(np.arange(ylimMin, ylimMax+ysteps, ysteps))
        plt.plot(dt_filtered,vel_abs,'o', color='black', label=r'$$v_{abs}$$')
        plt.plot(dt_filtered,vel_vx,'x', color='red', label=r'$$v_x$$')
        plt.plot(dt_filtered,vel_vy,'^', color='blue', label=r'$$v_y$$')
        plt.plot(0.575, 1.89655, '*', color = 'magenta', label = r'$$v_{OSM}$$')
        plt.legend()
        plt.xlim(xlimMin,xlimMax)
        plt.ylim(ylimMin,ylimMax)
        plt.xlabel(r'$t$ / s')
        plt.ylabel(r'$v$ / m/s')
        plt.grid()
        plt.tight_layout()

        plt.savefig(dd_out, dpi=300)
        print('Image saved to ', dd_out)
    # plt.close()
    return dt_filtered, vel_abs, vel_vx, vel_vy

global path 
if 0: # testStrobo
    filename = '/media/simon/Elements/AvaChute/2022_05_12_TestswithStrobo/C001C0207_20220512150814_0001/02_UpperChute/00_Flow_param5/C001C0207_20220512150814_0001.h5'
    path = '/media/simon/Elements/AvaChute/2022_05_12_TestswithStrobo/C001C0207_20220512150814_0001/frame1001.jpg'
    poi=np.array([1000,830])
    t, v_abs, _ , _ = readOFHDF5(filename, path, poi, keeptimestamp= False, saveFile = False)
    
    xlimMax = 2.25
    xlimMin = 0
    xsteps = 0.25
    
    ylimMax = 4
    ylimMin = -1
    ysteps = 1
    
    markersize=2
    
    plt.figure(figsize = nf.set_size(416,unit='pt'))
    plt.xticks(np.arange(xlimMin, xlimMax, xsteps))
    plt.yticks(np.arange(ylimMin, ylimMax, ysteps))
    plt.plot(t,v_abs,'o', markersize = markersize, color='black', label=r'$$v$$')
    # plt.legend()
    plt.xlim(xlimMin,xlimMax)
    plt.ylim(ylimMin,ylimMax)
    plt.xlabel(r'Time / s')
    plt.ylabel(r'Velocity / m/s')
    plt.grid()
    plt.tight_layout()
    
    dd_out = '/media/simon/Elements/AvaChute/2022_05_12_TestswithStrobo/C001C0207_20220512150814_0001/02_UpperChute/00_Flow_param5/00_Vabs_strobed.jpg'
    plt.savefig(dd_out, dpi=300)
    
if 0: # test with green strobo
    filename = '/home/simon/Documents/TestVideos/2022_05_30_BlueGreenStroboTests/C001C0215_20220530113707_0001/02_UpperChute/00_Flow/C001C0215_20220530113707_0001.h5'
    path = '/home/simon/Documents/TestVideos/2022_05_30_BlueGreenStroboTests/C001C0215_20220530113707_0001/02_UpperChute/frame1001.jpg'
    poi=np.array([350,830])
    readOFHDF5(filename,path, poi, keeptimestamp= False,saveFile = True)
    
if 0: # test permanent RGBW light#
    filename = '/media/simon/Elements/AvaChute/2022_05_31_OSM_Tests/C001C0221_20220531122829_0001/02_UpperChute/00_Flow_param5/C001C0221_20220531122829_0001.h5'
    path = '/media/simon/Elements/AvaChute/2022_05_31_OSM_Tests/C001C0221_20220531122829_0001/02_UpperChute/frame1001.jpg'
    poi=np.array([1000,815])
    readOFHDF5(filename,path, poi, keeptimestamp= False,saveFile = True)
    
if 1: # steady lighning final measurements
    filename = '/media/simon/radar_server/AvaChute/2022_06_14_FinalTests/C001C0225_20220614095036_0001/02_UpperChute/Setup_6/C001C0225_20220614095036_0001.h5'
    path = '/media/simon/radar_server/AvaChute/2022_06_14_FinalTests/C001C0225_20220614095036_0001/02_UpperChute/frame1001.jpg'
    poi=np.array([620 ,810])
    readOFHDF5(filename,path, poi, keeptimestamp= False,saveFile = True)
    
if 0: # create subplot of all 6 setups
    filename_setup1 = '/media/simon/radar_server/AvaChute/2022_06_14_FinalTests/C001C0225_20220614095036_0001/02_UpperChute/Setup_1/C001C0225_20220614095036_0001.h5'
    filename_setup2 = '/media/simon/radar_server/AvaChute/2022_06_14_FinalTests/C001C0225_20220614095036_0001/02_UpperChute/Setup_2/C001C0225_20220614095036_0001.h5'
    filename_setup3 = '/media/simon/radar_server/AvaChute/2022_06_14_FinalTests/C001C0225_20220614095036_0001/02_UpperChute/Setup_3/C001C0225_20220614095036_0001.h5'
    filename_setup4 = '/media/simon/radar_server/AvaChute/2022_06_14_FinalTests/C001C0225_20220614095036_0001/02_UpperChute/Setup_4/C001C0225_20220614095036_0001.h5'
    filename_setup5 = '/media/simon/radar_server/AvaChute/2022_06_14_FinalTests/C001C0225_20220614095036_0001/02_UpperChute/Setup_5/C001C0225_20220614095036_0001.h5'
    filename_setup6 = '/media/simon/radar_server/AvaChute/2022_06_14_FinalTests/C001C0225_20220614095036_0001/02_UpperChute/Setup_6/C001C0225_20220614095036_0001.h5'
    path = '/media/simon/radar_server/AvaChute/2022_06_14_FinalTests/C001C0225_20220614095036_0001/02_UpperChute/frame1053.jpg'
    poi=np.array([500+170,815])
    t, v_abs_setup1, vx_s1, vy_s1 = readOFHDF5(filename_setup1,path, poi, keeptimestamp= False, saveFile = False)
    _, v_abs_setup2, vx_s2, vy_s2 = readOFHDF5(filename_setup2,path, poi, keeptimestamp= False,saveFile = False)
    _, v_abs_setup3, vx_s3, vy_s3 = readOFHDF5(filename_setup3,path, poi, keeptimestamp= False,saveFile = False)
    _, v_abs_setup4, vx_s4, vy_s4 = readOFHDF5(filename_setup4,path, poi, keeptimestamp= False, saveFile = False)
    _, v_abs_setup5, vx_s5, vy_s5 = readOFHDF5(filename_setup5,path, poi, keeptimestamp= False,saveFile = False)
    _, v_abs_setup6, vx_s6, vy_s6 = readOFHDF5(filename_setup6,path, poi, keeptimestamp= False,saveFile = False)
    
    x_scale = np.arange(0, len(t)*2, 2)
    
    xlimMax = 4
    xlimMin = 0
    xsteps = 0.5
    
    ylimMax = 4
    ylimMin = -4
    ysteps = 1
    
    markersize=2
    
    rows, cols = 3, 2
    fig, ax = plt.subplots(rows, cols, sharex='col', sharey='row', figsize = nf.set_size(600,unit='pt'))
    # fig.tight_layout()
    fig.subplots_adjust(left=0.1, bottom=0.1, right=0.95, top=0.95, wspace=0.1, hspace=0.3)
    
    ax[0,0].plot(t, v_abs_setup1, 'o', markersize = markersize, color='black', label=r'$$v_{abs}$$')
    ax[0,0].plot(t, vx_s1, 'x', markersize = markersize, color='red', label=r'$$v_x$$')
    ax[0,0].plot(t, vy_s1, '^', markersize = markersize, color='blue', label=r'$$v_y$$')
    ax[0,0].set_xlim(xlimMin,xlimMax)
    ax[0,0].set_ylim(ylimMin,ylimMax)
    ax[0,0].set_xticks(np.arange(xlimMin,xlimMax+xsteps,step = xsteps))
    ax[0,0].set_yticks(np.arange(ylimMin,ylimMax+ysteps,step = ysteps))
    ax[0,0].grid()
    ax[0,0].set_title(r'a) Setup 1')
    ax[0,0].legend(loc = 'upper right')
    
    ax[0,1].plot(t, v_abs_setup2, 'o', markersize = markersize, color='black', label=r'$$v_{abs}$$')
    ax[0,1].plot(t, vx_s2, 'x', markersize = markersize, color='red', label=r'$$v_x$$')
    ax[0,1].plot(t, vy_s2, '^', markersize = markersize, color='blue', label=r'$$v_y$$')
    ax[0,1].set_xlim(xlimMin,xlimMax)
    ax[0,1].set_ylim(ylimMin,ylimMax)
    ax[0,1].set_xticks(np.arange(xlimMin,xlimMax+xsteps,step = xsteps))
    ax[0,1].set_yticks(np.arange(ylimMin,ylimMax+ysteps,step = ysteps))
    ax[0,1].grid()
    ax[0,1].set_title(r'b) Setup 2')
    ax[0,1].legend(loc = 'upper right')
    
    ax[1,0].plot(t, v_abs_setup3, 'o', markersize = markersize, color='black', label=r'$$v_{abs}$$')
    ax[1,0].plot(t, vx_s3, 'x', markersize = markersize, color='red', label=r'$$v_x$$')
    ax[1,0].plot(t, vy_s3, '^', markersize = markersize, color='blue', label=r'$$v_y$$')
    ax[1,0].set_xlim(xlimMin,xlimMax)
    ax[1,0].set_ylim(ylimMin,ylimMax)
    ax[1,0].set_xticks(np.arange(xlimMin,xlimMax+xsteps,step = xsteps))
    ax[1,0].set_yticks(np.arange(ylimMin,ylimMax+ysteps,step = ysteps))
    ax[1,0].grid()
    ax[1,0].set_title(r'c) Setup 3')
    ax[1,0].legend(loc = 'upper right')
    
    ax[1,1].plot(t, v_abs_setup4, 'o', markersize = markersize, color='black', label=r'$$v_{abs}$$')
    ax[1,1].plot(t, vx_s4, 'x', markersize = markersize, color='red', label=r'$$v_x$$')
    ax[1,1].plot(t, vy_s4, '^', markersize = markersize, color='blue', label=r'$$v_y$$')
    ax[1,1].set_xlim(xlimMin,xlimMax)
    ax[1,1].set_ylim(ylimMin,ylimMax)
    ax[1,1].set_xticks(np.arange(xlimMin,xlimMax+xsteps,step = xsteps))
    ax[1,1].set_yticks(np.arange(ylimMin,ylimMax+ysteps,step = ysteps))
    ax[1,1].grid()
    ax[1,1].set_title(r'd) Setup 4')
    ax[1,1].legend(loc = 'upper right')
    
    ax[2,0].plot(t, v_abs_setup5, 'o', markersize = markersize, color='black', label=r'$$v_{abs}$$')
    ax[2,0].plot(t, vx_s5, 'x', markersize = markersize, color='red', label=r'$$v_x$$')
    ax[2,0].plot(t, vy_s5, '^', markersize = markersize, color='blue', label=r'$$v_y$$')
    ax[2,0].set_xlim(xlimMin,xlimMax)
    ax[2,0].set_ylim(ylimMin,ylimMax)
    ax[2,0].set_xticks(np.arange(xlimMin,xlimMax+xsteps,step = xsteps))
    ax[2,0].set_yticks(np.arange(ylimMin,ylimMax+ysteps,step = ysteps))
    ax[2,0].grid()
    ax[2,0].set_title(r'e) Setup 5')
    ax[2,0].legend(loc = 'upper right')
    
    ax[2,1].plot(t, v_abs_setup6, 'o', markersize = markersize, color='black', label=r'$$v_{abs}$$')
    ax[2,1].plot(t, vx_s6, 'x', markersize = markersize, color='red', label=r'$$v_x$$')
    ax[2,1].plot(t, vy_s6, '^', markersize = markersize, color='blue', label=r'$$v_y$$')
    ax[2,1].set_xlim(xlimMin,xlimMax)
    ax[2,1].set_ylim(ylimMin,ylimMax)
    ax[2,1].set_xticks(np.arange(xlimMin,xlimMax+xsteps,step = xsteps))
    ax[2,1].set_yticks(np.arange(ylimMin,ylimMax+ysteps,step = ysteps))
    ax[2,1].grid()
    ax[2,1].set_title(r'f) Setup 6')
    ax[2,1].legend(loc = 'upper right')
    
    for a in ax.flat:
        a.set(xlabel='$t$ / s', ylabel='$v$ / m/s')
        a.label_outer()
    
    
    if 1: #save setup comparison
        
        dd_out = '/media/simon/radar_server/AvaChute/2022_06_14_FinalTests/C001C0225_20220614095036_0001/02_UpperChute/00_SetupComparison.jpg'
    
        plt.savefig(dd_out, dpi=300)
        print('Image saved to ', dd_out)
    
    
if 0: #compare maximal veelocity
    filename_setup1 = '/media/simon/radar_server/AvaChute/2022_06_14_FinalTests/C001C0225_20220614095036_0001/02_UpperChute/FindMaxVelocitySetup1/C001C0225_20220614095036_0001.h5'
    filename_setup2 = '/media/simon/radar_server/AvaChute/2022_06_14_FinalTests/C001C0225_20220614095036_0001/02_UpperChute/FindMaxVelocitySetup2/C001C0225_20220614095036_0001.h5'
    filename_setup3 = '/media/simon/radar_server/AvaChute/2022_06_14_FinalTests/C001C0225_20220614095036_0001/02_UpperChute/FindMaxVelocitySetup3/C001C0225_20220614095036_0001.h5'
    filename_setup4 = '/media/simon/radar_server/AvaChute/2022_06_14_FinalTests/C001C0225_20220614095036_0001/02_UpperChute/FindMaxVelocitySetup4/C001C0225_20220614095036_0001.h5'
    filename_setup5 = '/media/simon/radar_server/AvaChute/2022_06_14_FinalTests/C001C0225_20220614095036_0001/02_UpperChute/FindMaxVelocitySetup5/C001C0225_20220614095036_0001.h5'
    filename_setup6 = '/media/simon/radar_server/AvaChute/2022_06_14_FinalTests/C001C0225_20220614095036_0001/02_UpperChute/FindMaxVelocitySetup6/C001C0225_20220614095036_0001.h5'
    path = '/media/simon/radar_server/AvaChute/2022_06_14_FinalTests/C001C0225_20220614095036_0001/02_UpperChute/frame1053.jpg'
    poi=np.array([1200,815])
    t, v_abs_setup1, _, _ = readOFHDF5(filename_setup1,path, poi, keeptimestamp= True, saveFile = False)
    _, v_abs_setup2, _, _ = readOFHDF5(filename_setup2,path, poi, keeptimestamp= True,saveFile = False)
    _, v_abs_setup3, _, _ = readOFHDF5(filename_setup3,path, poi, keeptimestamp= True,saveFile = False)
    _, v_abs_setup4, _, _ = readOFHDF5(filename_setup4,path, poi, keeptimestamp= True, saveFile = False)
    _, v_abs_setup5, _, _ = readOFHDF5(filename_setup5,path, poi, keeptimestamp= True,saveFile = False)
    _, v_abs_setup6, _, _ = readOFHDF5(filename_setup6,path, poi, keeptimestamp= True,saveFile = False)
    
    x_scale = np.arange(0, len(t)*2, 2)
    
    xlimMax = 70
    xlimMin = 0
    xsteps = 4
    
    ylimMax = 7
    ylimMin = -1
    ysteps = 1
    
    plt.figure(figsize = nf.set_size(416,unit='pt'))
    plt.xticks(np.arange(xlimMin, xlimMax, xsteps))
    plt.yticks(np.arange(ylimMin, ylimMax, ysteps))
    plt.plot(x_scale,v_abs_setup1,'-', label=r'$$v_{setup1}$$')
    plt.plot(x_scale,v_abs_setup2,'-o', label=r'$$v_{setup2}$$')
    plt.plot(x_scale,v_abs_setup3,'-x',  label=r'$$v_{setup3}$$')
    plt.plot(x_scale,v_abs_setup4,'--', label=r'$$v_{setup4}$$')
    plt.plot(x_scale,v_abs_setup5,'-1', label=r'$$v_{setup5}$$')
    plt.plot(x_scale,v_abs_setup6,'-^', label=r'$$v_{setup6}$$')
    plt.legend()
    plt.xlim(xlimMin,xlimMax)
    plt.ylim(ylimMin,ylimMax)
    plt.xlabel(r'$n_{sk.f.}$ / 1')
    plt.ylabel(r'$v_{abs}$ / m/s')
    plt.grid()
    plt.tight_layout()
    
    if 1: #save max velocity comparison
        
        name=filename_setup1.split('.')
        name=np.delete(name[0].split('/'),-1)
        save_velProfileTo='MaximalVelocities.jpg'
        dd_out="/".join(np.append(name,save_velProfileTo))
    
        plt.savefig(dd_out, dpi=300)
        print('Image saved to ', dd_out)
    
    
    
        

# define point of interest

# poi=np.array([1300,830]) # point of interest lower chute