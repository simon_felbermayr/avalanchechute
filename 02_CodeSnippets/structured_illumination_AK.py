import os
import numpy as np
import cv2

os.environ.pop("QT_QPA_PLATFORM_PLUGIN_PATH", None) # https://stackoverflow.com/a/67863156

import glob
#import matplotlib as mpl
import matplotlib.pyplot as plt
import skimage.io
from skimage.color import rgb2gray
import numpy_indexed as npi
import math 

plt.close('all')

def max_rgb_filter(fn):
	# split the image into its BGR components
    frame = cv2.imread(fn)[636:1150,488:1794]
    (B, G, R) = cv2.split(frame)
	# find the maximum pixel intensity values for each
	# (x, y)-coordinate,, then set all pixel values less
	# than M to zero
    M = np.maximum(np.maximum(R, G), B)
    R[R < M] = 0
    G[G < M] = 0
    B[B < M] = 0
	# merge the channels back together and return the image
    image=cv2.merge([B, G, R])
    plt.figure()
    plt.imshow(image)

def color_filter(fn, typ):
    frame = cv2.imread(fn)[636:1150,488:1794]
    plt.figure()
    plt.imshow(frame)

    '''Getting rid of noise in image'''
    f_size=25

    print(frame.dtype)

    # plt.figure()
    # plt.imshow(frame)
    # transformed_channels = []
    # for i in range(3):
    #     rgb_fft = np.fft.fftshift(np.fft.fft2((frame[:, :, i])))
        
       

        
    #     # find DC part and whipe out 10 % around it to get rid of high freq signals
    #     dcPoint=np.where(rgb_fft == np.amax(rgb_fft))

    #     sizeGrayscale=rgb_fft.shape

    #     filtPercent=1 # %
    #     filtPixels=int(np.floor(sizeGrayscale[0]*filtPercent/100))

    #     start=dcPoint[0]-filtPixels
    #     end=dcPoint[0]+filtPixels
        
        
    #     rgb_fft[start[0]:end[0], :int(np.ceil(sizeGrayscale[1]/2))] = 1
    #     rgb_fft[start[0]:end[0],-int(np.ceil(sizeGrayscale[1]/2)):] = 1
        
    #     transformed_channels.append(abs(np.fft.ifft2(rgb_fft)))
        
    #     fig, ax = plt.subplots(1,3,figsize=(15,15))
    #     ax[0].imshow(np.log(abs(rgb_fft)))
    #     ax[0].set_title('Masked Fourier', fontsize = f_size)
    #     ax[1].imshow(rgb2gray(frame))
    #     ax[1].set_title('Greyscale Image', fontsize = f_size);
    #     ax[2].imshow(abs(np.fft.ifft2(rgb_fft)))
    #     ax[2].set_title('Greyscale Image with Mask', fontsize = f_size);
    
    # final_image = np.dstack([transformed_channels[0].astype(np.uint8), 
    #                           transformed_channels[1].astype(np.uint8), 
    #                           transformed_channels[2].astype(np.uint8)])
    
    # print(final_image.dtype)
    
    # fig, ax = plt.subplots(1, 2, figsize=(17,12))
    # ax[0].imshow(frame)
    # ax[0].set_title('Original Image', fontsize = f_size)
    # ax[0].set_axis_off()
    
    # ax[1].imshow(final_image)
    # ax[1].set_title('Transformed Image', fontsize = f_size)
    # ax[1].set_axis_off()
    
    # fig.tight_layout()
    
    hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
    
    plt.figure()
    plt.imshow(hsv)

    if typ == 'dark':
        # lower boundary RED color range values
        lower1 = np.array([0, 0, 170])
        upper1 = np.array([50, 255, 255])

        # upper boundary RED color range values
        lower2 = np.array([150, 40, 170])
        upper2 = np.array([180, 255, 255])
    elif typ == 'bright':
        lower1 = np.array([0, 15, 10])
        upper1 = np.array([50, 50, 255])

        lower2 = np.array([130, 15, 10])
        upper2 = np.array([180, 50, 255])

    lower_mask = cv2.inRange(hsv, lower1, upper1)
    upper_mask = cv2.inRange(hsv, lower2, upper2)

    mask = lower_mask + upper_mask

    res = cv2.bitwise_and(frame, frame, mask=mask)

    final = cv2.cvtColor(res, cv2.COLOR_BGR2RGB)
    return final, hsv


if __name__ == '__main__':
    
    fig, axs = plt.subplots(4, 1, sharex=True, sharey=True)
    nr = -2
    for typ in ['dark', 'bright']:
        nr = nr + 2
        if typ == 'dark':
            fn = "/home/simon/Documents/TestVideos/2022_04_22_Tests_AllSensorsCam/C001C0176_20220422155323_0001/frame666.jpg"
        elif typ == 'bright':
            fn = "/home/simon/Documents/TestVideos/2022_04_20_TestLaserAppearance_LEDStrahlerLEDStripe/C001C0160_20220421143844_0001/frame110.jpg"
        final, hsv = color_filter(fn, typ)

        axs[nr].imshow(hsv)
        axs[nr+1].imshow(final)
        axs[nr].set_ylabel('hsv: '+typ)
        axs[nr+1].set_ylabel(typ)
    plt.show()
    #cv2.imshow('frame', frame)
    #cv2.imshow('mask', mask)
    #cv2.imshow('res', res)

    #k = cv2.waitKey()


    #cv2.destroyAllWindows()
