# -*- coding: utf-8 -*-
"""
Created on Mon Feb 21 11:12:14 2022

@author: Felbermayr Simon
"""
from __future__ import unicode_literals, print_function
from openpiv import tools, pyprocess, validation, filters, scaling, preprocess


from openpiv import tools, pyprocess, validation, filters, scaling
from openpiv.piv import simple_piv
from pathlib import Path

import numpy as np
import matplotlib.pyplot as plt
#%matplotlib inline
import imageio
import cv2 
import os 
import glob
import openpiv
import ffmpeg
import sys
import time
from pathlib import Path
import pdb

from PIL import Image
import scipy.signal as sp


plt.close('all')

########## frame a
frame_a=tools.imread("/home/simon/Documents/TestVideos/2022_05_12_TestswithStrobo/C001C0207_20220512150814_0001/02_UpperChute/frame1054.jpg")

########## frame b
frame_b=tools.imread("/home/simon/Documents/TestVideos/2022_05_12_TestswithStrobo/C001C0207_20220512150814_0001/02_UpperChute/frame1057.jpg")

plt.figure()
plt.imshow(frame_a)

# (thresh, frame_a) = cv2.threshold(frame_a, 70, 255, cv2.THRESH_BINARY)
# (thresh, frame_b) = cv2.threshold(frame_b, 70, 255, cv2.THRESH_BINARY)

plt.figure()
plt.imshow(frame_b)

# ret, frame_a = cv2.threshold(frame_a, 70, 255, cv2.THRESH_BINARY)
# ret, frame_b = cv2.threshold(frame_b, 70, 255, cv2.THRESH_BINARY)

plt.imshow(np.c_[frame_a,frame_b],cmap='gray')

# frame_a=frame_a[429:1207,1855:3806]
# frame_b=frame_b[429:1207,1855:3806]

# cv2.imwrite("/home/simon/Documents/AvalancheChute/01_PIV/frames_B005C0121_20220412152347_0001/cropped_frame138.jpg",frame_a)
# cv2.imwrite("/home/simon/Documents/AvalancheChute/01_PIV/frames_B005C0103_20220407150600_0001/cropped_frame966.jpg",frame_b)


# plt.imshow(np.c_[frame_a,frame_b],cmap='gray')

# # masking using optimal (manually tuned) set of parameters and the right method:
# frame_a, _ = preprocess.dynamic_masking(frame_a,method='edges',filter_size=7,threshold=0.01)
# frame_b, _ = preprocess.dynamic_masking(frame_b,method='edges',filter_size=7,threshold=0.01)
# plt.figure()
# plt.imshow(np.c_[frame_a,frame_b],cmap='gray')




fig,ax = plt.subplots(1,2,figsize=(12,10))
ax[0].imshow(frame_a,cmap=plt.cm.gray)
ax[1].imshow(frame_b,cmap=plt.cm.gray)

winsize = 32    # pixels, interrogation window size in frame A
searchsize = 128 # pixels, search in image B
overlap = 16   # pixels, 50% overlap
dt = 0.038      # sec, time interval between pulses calc with frames and video time


u0, v0, sig2noise = pyprocess.extended_search_area_piv(frame_a.astype(np.int32), 
                                                       frame_b.astype(np.int32), 
                                                       window_size=winsize, 
                                                       overlap=overlap, 
                                                       dt=dt, 
                                                       search_area_size=searchsize, 
                                                       sig2noise_method='peak2peak')


x, y = pyprocess.get_coordinates( image_size=frame_a.shape, 
                                 search_area_size=searchsize, 
                                 overlap=overlap )


u1, v1, mask = validation.sig2noise_val( u0, v0, 
                                        sig2noise, 
                                        threshold = 0.5)#np.percentile(sig2noise,2.5) )
# u1, v1, mask = validation.global_val( u1, v1, (-100, 100), (-10, 10) )

# histogram of sig2noise
plt.figure()
plt.hist(sig2noise.flatten())
# to see where is a reasonable limit
# filter out outliers that are very different from the
# neighbours

u2, v2 = filters.replace_outliers( u1, v1, 
                                  method='localmean', 
                                  max_iter=10, 
                                  kernel_size=2)
# convert x,y to mm
# convert u,v to mm/sec

x, y, u3, v3 = scaling.uniform(x, y, u2, v2, 
                               scaling_factor = 96.52 ) # 96.52 microns/pixel

# 0,0 shall be bottom left, positive rotation rate is counterclockwise
x, y, u3, v3 = tools.transform_coordinates(x, y, u3, v3)
#save in the ASCII table format
tools.save(x, y, u3, v3, mask, 'exp1_001.txt' )


fig, ax = plt.subplots(figsize=(8,8))
tools.display_vector_field('exp1_001.txt', 
                           ax=ax, scaling_factor=96.52, 
                           scale=10, # scale defines here the arrow length
                           width=0.0035, # width is the thickness of the arrow
                           on_img=True, # overlay on the image)
                           image_name= "/home/simon/Documents/TestVideos/2022_05_12_TestswithStrobo/C001C0207_20220512150814_0001/02_UpperChute/frame1057.jpg")
fig.savefig(r'../01_PIV/vector_plots/piv_vel'+'.png',bbox_inches='tight')