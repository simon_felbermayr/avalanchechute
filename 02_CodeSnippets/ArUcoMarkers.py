#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May 12 08:50:44 2022

@author: simon
"""

import numpy as np
import cv2, PIL
from cv2 import aruco
import matplotlib.pyplot as plt
import matplotlib as mpl
import pandas as pd
import glob
import os
# %matplotlib nbagg

plt.close('all')

plt.rcParams['text.usetex'] = True

createArUco = False
showDetailedData = True
showChuteOscillations = True

def saveFigure(path, addToName= None): 
        name=path.split('.')
        filename = name[0].split('/')
        folder = np.delete(filename,-1)
        savefig_type = '.jpg'
        seperator = '/'
        folder = seperator.join(folder)
        dd_out = folder
        
        if not os.path.exists(dd_out):
            os.makedirs(dd_out)
        
        if addToName != None:
            dd_out=dd_out+filename[-1] + '_oscillations'+ addToName + savefig_type
        else:
            dd_out=dd_out+filename[-1] + '_oscillations'+ savefig_type
        
        plt.savefig(dd_out, dpi=700)
        # plt.close()
        
        print('Saved image to: ',dd_out)

def getTimestamp(pathImg):
    
    name=pathImg.split('.')
    filename = name[0].split('/')
    
    # calc timestamp
    dt = int(filename[-1].replace('frame',''))
    
    # frames per second ZCam
    fps = 160
    T = 1/fps
    
    timestamp=str(round((dt-1000)*T,5)) # -1000 because save names;
    
    return timestamp

if 0: # create ArUcos characters
    aruco_dict = aruco.Dictionary_get(aruco.DICT_6X6_250)
    
    if 1: # show all created markers
        
        width = 12
        height = 1.5
        
        sizeFigure = (width, height)
        
        fig = plt.figure(figsize = sizeFigure)
        
        nx = 8
        ny = 1
        for i in range(1, nx*ny+1):
            ax = fig.add_subplot(ny,nx, i)
            img = aruco.drawMarker(aruco_dict,i+4, 700)
            plt.imshow(img, cmap = mpl.cm.gray, interpolation = "nearest")
            ax.axis("off")
            id_name = 'ID: '+ str(i+4)
            ax.set_title(id_name)
        fig.tight_layout()
        plt.savefig("ArUCoMarkers/markers.jpg", dpi = 300)
        plt.show()
        
    if 0: # show one specific marker
        plt.figure()
        img = aruco.drawMarker(aruco_dict,1, 700)
        plt.imshow(img, cmap = mpl.cm.gray, interpolation = "nearest")
        plt.axis('off')
        plt.tight_layout()
        plt.savefig('/home/simon/Documents/Thesis/3_figures/ArUcoMarker.jpg', dpi=300)



def findArUcos(path, showMarkers = False):
    
    frame=cv2.imread(path)
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    aruco_dict = aruco.Dictionary_get(aruco.DICT_6X6_250)
    parameters =  aruco.DetectorParameters_create()
    corners, ids, rejectedImgPoints = aruco.detectMarkers(gray, aruco_dict, parameters=parameters)
    
    if showMarkers:
        frame_markers = aruco.drawDetectedMarkers(frame.copy(), corners, ids)
        return corners, ids, frame_markers
    else:
        return corners, ids
    
def getOscillations(fns):
    
    oscillations_x = []
    oscillations_y = []
    t = []
    
    start_idx = 400
    end_idx = 1300
    
    for i in range(start_idx,end_idx-1):
        absoluteTime = getTimestamp(fns[i])

        try:
            corners, ids = findArUcos(fns[i])
            
            x_pos = corners[np.where(ids==9)[0][0]][0][0][0]
            y_pos = corners[np.where(ids==9)[0][0]][0][0][1]
            
            oscillations_x.append(x_pos)
            oscillations_y.append(y_pos)
            t.append(float(absoluteTime))
        except:
            print('Did not work for timestamp: ', absoluteTime)
            
    return oscillations_x, oscillations_y, t
    
if 0: # show ArUco markers
    corners, ids, frame_markers = findArUcos('/media/simon/Elements/AvaChute/2022_05_12_TestswithStrobo/C001C0205_20220512140139_0001/frame1000.jpg', True)
    plt.figure()
    plt.imshow(frame_markers)
    
    c = corners[0][0]
    plt.annotate('ID =12', 
             (c[:, 0].mean(), c[:, 1].mean()), # these are the coordinates to position the label
             color='black',
             size=10)
    
    c = corners[1][0]
    plt.annotate('ID =11', 
             (c[:, 0].mean(), c[:, 1].mean()), # these are the coordinates to position the label
             color='black',
             size=10)
    
    c = corners[2][0]
    plt.annotate('ID =9', 
             (c[:, 0].mean(), c[:, 1].mean()), # these are the coordinates to position the label
             color='black',
             size=10)
    
    c = corners[3][0]
    plt.annotate('ID =10', 
             (c[:, 0].mean(), c[:, 1].mean()), # these are the coordinates to position the label
             color='black',
             size=10)
    
    c = corners[4][0]
    plt.annotate('ID =8', 
             (c[:, 0].mean(), c[:, 1].mean()), # these are the coordinates to position the label
             color='black',
             size=10)
    
    c = corners[5][0]
    plt.annotate('ID =7', 
             (c[:, 0].mean(), c[:, 1].mean()), # these are the coordinates to position the label
             color='black',
             size=10)
    
    c = corners[6][0]
    plt.annotate('ID =6', 
             (c[:, 0].mean(), c[:, 1].mean()), # these are the coordinates to position the label
             color='black',
             size=10)
    
    c = corners[7][0]
    plt.annotate('ID =5', 
             (c[:, 0].mean(), c[:, 1].mean()), # these are the coordinates to position the label
             color='black',
             size=10)
    
    for i in range(len(ids)):
        c = corners[i][0]
        plt.plot([c[:, 0].mean()], [c[:, 1].mean()], "o", label = "id={0}".format(ids[i]))
    plt.legend()
    plt.show()

    # index for ArUco
    
    # upper chute
    # id11 upper right
    # id12 lower right
    # id9  upper left
    # id10 lower left
    
    # lower chute
    # id8 upper right
    # id7 lower right
    # id6 upper left
    # id5 lower left
    
    # save always top right corner od ArUco to change perspective
    
    upperright_up=corners[np.where(ids==11)[0][0]][0][0]
    lowerright_up=corners[np.where(ids==12)[0][0]][0][0]
    upperleft_up=corners[np.where(ids==9)[0][0]][0][0]
    lowerleft_up=corners[np.where(ids==10)[0][0]][0][0]
    
    # if whole chute is read uncomment below lines
    # upperright_low=corners[np.where(ids==8)[0][0]][0][0]
    # lowerright_low=corners[np.where(ids==7)[0][0]][0][0]
    # upperleft_low=corners[np.where(ids==6)[0][0]][0][0]
    # lowerleft_low=corners[np.where(ids==5)[0][0]][0][0]

if 1: # showChuteOscillations

    folder_handRelease = '/media/simon/radar_server/AvaChute/2022_06_14_FinalTests/C001C0229_20220614100738_0001/' # 02_UpperChute/'
    folder_codeRelease = '/media/simon/radar_server/AvaChute/2022_06_14_FinalTests/C001C0230_20220614101014_0001/'
    # folder = '/media/simon/Elements/AvaChute/2022_06_14_FinalTests/C001C0230_20220614101014_0001/'
    path_handRelease = folder_handRelease+"*.jpg"
    path_codeRelease = folder_codeRelease+"*.jpg"
    
    fns_handRelease = sorted(glob.glob(path_handRelease))
    fns_codeRelease = sorted(glob.glob(path_codeRelease))
    
    x_handRelease, y_handRelease, t_handRelease = getOscillations(fns_handRelease)
    x_codeRelaese, y_codeRelease, t_codeRelease = getOscillations(fns_codeRelease)
    
    for i in range(len(t_codeRelease)):
        t_handRelease[i] = t_handRelease[i]-2.5
        t_codeRelease[i] = t_codeRelease[i]-2.5
    
    #plt.figure()
    fig, axs = plt.subplots(2)
    
    #plt.subplots_adjust(left=None, bottom=None, right=None, top=None, wspace=0, hspace=0.5)
    
    # plt.axis('off')
    plt.setp(axs, xticks=np.arange(0, t_handRelease[len(t_handRelease)-1],0.2))

    # fig.suptitle(r'Timestamp: '+timestamp+' ms', fontsize=16)
    axs[0].set_title(r'Oscillations Y-direction')
    axs[1].set_title(r'Oscillations X-direction')

    minY = 443
    maxY = 450
    
    minX = 0
    maxX = 2.5
    # image
    sub1=axs[0].plot(t_handRelease, y_handRelease, '-k')
    axs[0].set_xlabel(r'$t$ / ms')
    axs[0].set_ylabel(r'$p_{ArUco}$ / pixel')
    axs[0].set_title(r'a) Hand release of granulate inlet')
    axs[0].set_ylim(minY,maxY)
    axs[0].set_xlim(minX,maxX)    
    axs[0].grid()
    # vx component
    sub2=axs[1].plot(t_codeRelease, y_codeRelease, '-k')
    axs[1].set_xlabel(r'$t$ / ms')
    axs[1].set_ylabel(r'$p_{ArUco}$ / pixel')
    axs[1].set_title(r'b) Automated release of granulate inlet')
    axs[1].set_ylim(minY,maxY)
    axs[1].set_xlim(minX,maxX)  
    axs[1].grid()
    fig.tight_layout()
    
    
    
    saveFigure(fns_handRelease[0])
    
    
if 0: #show details of outcome
    def quad_area(data):
        l = data.shape[0]//2
        corners = data[["c1", "c2", "c3", "c4"]].values.reshape(l, 2,4)
        c1 = corners[:, :, 0]
        c2 = corners[:, :, 1]
        c3 = corners[:, :, 2]
        c4 = corners[:, :, 3]
        e1 = c2-c1
        e2 = c3-c2
        e3 = c4-c3
        e4 = c1-c4
        a = -.5 * (np.cross(-e1, e2, axis = 1) + np.cross(-e3, e4, axis = 1))
        return a
    
    corners2 = np.array([c[0] for c in corners])
    
    data = pd.DataFrame({"x": corners2[:,:,0].flatten(), "y": corners2[:,:,1].flatten()},
                       index = pd.MultiIndex.from_product(
                               [ids.flatten(), ["c{0}".format(i )for i in np.arange(4)+1]],
                           names = ["marker", ""] ))
    
    data = data.unstack().swaplevel(0, 1, axis = 1).stack()
    data["m1"] = data[["c1", "c2"]].mean(axis = 1)
    data["m2"] = data[["c2", "c3"]].mean(axis = 1)
    data["m3"] = data[["c3", "c4"]].mean(axis = 1)
    data["m4"] = data[["c4", "c1"]].mean(axis = 1)
    data["o"] = data[["m1", "m2", "m3", "m4"]].mean(axis = 1)
    data