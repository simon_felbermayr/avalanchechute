# -*- coding: utf-8 -*-
"""
Created on Thu Mar 17 08:40:55 2022

@author: admin
"""


import PySimpleGUI as gui

layout = [[gui.Text("Measurement")], [gui.Button("Start"),gui.Button("Cancel")]]
layout1 = [[gui.Text("Measurement")], [gui.Button("Stop")]]

# Create the window
window = gui.Window("Avalanche Chute", layout, size=(290, 80))
windowMeasStarted=gui.Window("Avalanche Chute", layout1, size=(290, 80))

# Create an event loop
while True:
    event, values = window.read()
    if event == "Start" or event == gui.WIN_CLOSED:
        window.close()
        break
        
while True:        
    event, values = windowMeasStarted.read()   
    if event == "Stop" or event == gui.WIN_CLOSED:
         windowMeasStarted.close()
         break
     
