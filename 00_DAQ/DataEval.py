# -*- coding: utf-8 -*-
"""
Created on Thu Mar 31 10:55:55 2022

@author: admin
"""

import sys
# insert at 1, 0 is the script path (or '' in REPL)

my_os=sys.platform
if my_os == 'win32':
    sys.path.insert(1, 'C:/Users/admin/Documents/avalanchechute/02_CodeSnippets/')
else:
    sys.path.insert(1, '/home/simon/Documents/AvalancheChute/02_CodeSnippets/')

import glob
import numpy as np
import matplotlib.pyplot as plt
import h5py
import os
from scipy import signal
from scipy.signal import butter, lfilter, freqz
import nice_figures as nf
import math

# plt.close('all')
plt.rcParams['text.usetex'] = True
plt.rcParams['font.size'] = 12

def hdf5_load_parameter(filename, group_name='parameter'):
    f = h5py.File(filename, 'r')
    param_grp = f[group_name]
    dictionary = {}
    for key in param_grp.keys():
        if isinstance(param_grp[key], h5py._hl.group.Group):
            # if there is a subgroup, recursively load to dict
            dictionary[key] = hdf5_load_parameter(filename, group_name+'/'+key)
        else:
            dictionary[key] = param_grp[key][()]
            if isinstance(dictionary[key], bytes):
                # assume, the new version of h5py read string as bytes
                dictionary[key] = dictionary[key].decode("utf-8")
    return dictionary


def hdf5_load_array(filename, fieldname='data'):
    #print(filename)
    f = h5py.File(filename, 'r')
    if f.get(fieldname) is not None:
        ds = f[fieldname]
        array = ds[()]
    else:
        return -1
    return array


def load_DAQ_data(filename):
    ext = os.path.splitext(filename)
    if ext[1] == '.h5':
        data = hdf5_load_array(filename)
        parameter = hdf5_load_parameter(filename)
    elif ext[1] == '.txt' or ext[1] == '.csv':
        data = np.loadtxt(filename, delimiter=',', dtype=float)
        parameter = None
    else:
        Warning('Dont know how to open: ' + filename)


    return data, parameter    

def saveFigure(path, nr, addString): 
        name=path.split('.')
        filename = name[0].split('/')
        folder = np.delete(filename,-1)
        savefig_type = '.jpg'
        seperator = '/'
        folder = seperator.join(folder)
        dd_out = folder +'/OSM_signal/'
        
        if not os.path.exists(dd_out):
            os.makedirs(dd_out)
        
        if addString != None:
            dd_out=dd_out+filename[-1]+whichOSM(nr)+addString+savefig_type
        else:
            dd_out=dd_out+filename[-1]+whichOSM(nr)+savefig_type
            
        plt.tight_layout()
        plt.savefig(dd_out, dpi=300)
        # plt.close()
        
        print('Saved image to: ',dd_out)

def plot():
    fig, ax = plt.subplots(2, 4, sharex='all', sharey='all')
    ax[0][0].plot(time,data[:,0])
    ax[0][0].grid()
    ax[1][0].plot(time,data[:,1])
    ax[1][0].grid()
    ax[0][1].plot(time,data[:,2])
    ax[0][1].grid()
    ax[1][1].plot(time,data[:,3])
    ax[1][1].grid()
    ax[0][2].plot(time,data[:,4])
    ax[0][2].grid()
    ax[1][2].plot(time,data[:,5])
    ax[1][2].grid()
    ax[0][3].plot(time,data[:,6])
    ax[0][3].grid()
    ax[1][3].plot(time,data[:,7])
    plt.setp(ax, ylim=(-5,5))
    ax[1][3].grid()

def plotSingleOSMVoltage(data = None, OSM_number = 0, x_range = None, markevery = None, nameOfFile = None, addString = None):
    global time
    
    set_2=data[:,OSM_number+1]
    set_1=data[:,OSM_number]
    
    labelChA = whichOSM(OSM_number) + ' Channel A'
    labelChB = whichOSM(OSM_number) + ' Channel B'
    
    # hard coded time shift since measurement system (DAQ) starts to late
    time = time# + 0.2
    
    plt.figure(figsize = nf.set_size(416,unit='pt'))
    plt.plot(time, set_2,ls = '-', color='gray', linewidth=1.5, label = labelChA) # m'arker ='.', markevery = 50,
    plt.plot(time, set_1,ls ='--',color='k', marker = 'o', fillstyle='none',ms = 4,linewidth=0.5, markevery = markevery, alpha=1,  label = labelChB)
    plt.ylim(-0.7,0.7)
    plt.xlim(0,8)
    
    if x_range != None:
        plt.xlim(x_range[0],x_range[1])
        
    
    # limits for C0221 signal
    # plt.xlim(0.5,0.8)
    # plt.ylim(-0.8,0.3)
    plt.grid()
    plt.legend()
    plt.xlabel(r'$t$ / s')
    plt.ylabel(r'$u$ / V')
    saveFigure(nameOfFile, OSM_number, addString)

    
def plotPointLaserHeight(data = None):
    
    baluff_1 = (data[:,6]-1.55)/0.02*(-1)*math.cos(13)
    baluff_2 = (data[:,7]-1.55)/0.02*(-1)*math.cos(13)
    
    baluff_1 = data[:,6]/0.02-1.55
    baluff_2 = data[:,7]/0.02-1.55
    
    sampling_rate = 20e3
    timeStamp=1/sampling_rate
    time = np.arange(0,timeStamp*len(data),timeStamp) 
    
    plt.figure()
    # plt.plot(baluff_1,'-',  label = 'Baluff 1') # marker ='.', markevery = 50,
    plt.plot(time, baluff_2, label = 'Baluff 2')
    plt.grid()
    plt.legend()
    plt.xlabel(r'Time / s')
    plt.ylabel(r'Height / mm')
    
    # out of data 0,1V is 5cm so factor is 50

def whichOSM(i):
         switcher={
                0:"OSM1",
                2:"OSM2",
                4:"OSM3"
                }
         return switcher.get(i,"Invalid counter variable")

def butter_lowpass(cutoff, fs, order=5):
    nyq = 0.5 * fs
    normal_cutoff = cutoff / nyq
    b, a = butter(order, normal_cutoff, btype='low', analog=False)
    return b, a

def butter_lowpass_filter(data, cutoff, fs, order=5):
    b, a = butter_lowpass(cutoff, fs, order=order)
    y = lfilter(b, a, data)
    return y

            
def xcorr():
    global data
    global sampling_rate
    maxVel=10 #m/s
    sensor_gap=11e-3 # m
    
    #############################
    # Overall calculation of cross correlation
    # delete all nans
    if np.isnan(data).any():
        data=data[~np.isnan(data).any(axis=1), :]
        
    fig, ax1 = plt.subplots(1, 3, sharex='all', sharey='all')
    j=0 #counter variable for plot
        
    for i in range(0,5,2):
        
        
        
        
        set_2=data[:,i+1]
        set_1=data[:,i]
        threshold_OSM=0.05
    
        # next(x[0] for x in enumerate(L) if x[1] > 0.7)
        
        
        # # Filtering and plotting
        # cutoff = 4
        # fs = 10
        # order = 2
        
        # set_1 = butter_lowpass_filter(set_1, cutoff, fs, order)
        # set_2 = butter_lowpass_filter(set_2, cutoff, fs, order)
        
        # set_1 = set_1
        # set_2 = set_2
        
        # labelChA = whichOSM(i) + ' Channel A'
        # labelChB = whichOSM(i) + ' Channel B'
        
        # plt.figure()
        # plt.plot(set_1, '', label = labelChA)
        # plt.plot(set_2, marker ='.', markevery = 1, label = labelChB)
        # plt.grid()
        # plt.legend()
        # plt.xlabel(r'Time / s')
        # plt.ylabel(r'Voltage / V')
        # # plt.title("Filtered")
        
        
        # check data if a object passed the OSM
        if max(set_1)>threshold_OSM and max(set_2)>threshold_OSM:
            # cross correlation
            
            print('Time of front velocity: ', np.argmax(set_2<(-threshold_OSM))/sampling_rate)
            
            '''calculate with numpy module'''
            correlate_result=np.correlate(set_1, set_2,'full')
            
            maxCorr=np.amax(correlate_result)
            
            index_maxCorr = np.where(correlate_result == maxCorr)
            time_lag = (index_maxCorr[0]-len(set_1))/sampling_rate
            velocity=sensor_gap/time_lag
            
            ''' calculate lag with scipy module '''
            correlation = signal.correlate(set_1-np.mean(set_1), set_2 - np.mean(set_2), mode="full")
            # lags = signal.correlation_lags(len(set_1), len(set_2), mode="full")
            time_lag_scipy = (np.argmax(abs(correlation))-len(set_1))/sampling_rate
            velocity_scipy=sensor_gap/time_lag_scipy
            print("length: ",len(set_1))
            print("Time lag Scipy: ", time_lag_scipy, "Velocity: ", velocity_scipy)
            if 0: # plot correlation
                plt.figure()
                plt.plot(abs(correlation))
                plt.plot(abs(signal.correlate(set_1, set_2, mode="full")))
                plt.plot(abs(signal.correlate(np.ones_like(set_1)*np.mean(set_1), np.ones_like(set_2)*np.mean(set_2))))
                plt.legend(['subtracting mean', 'constant signal', 'keeping the mean'])
            
            # plt.figure()
            # plt.plot(correlate_result)
            # plt.grid()
            
            if velocity>maxVel:
                print("Velocity exits maximum of "+ str(maxVel))
            else:
                ax1[j].plot(correlate_result)
                j=j+1
                0
            print(whichOSM(i), "Time lag: ", time_lag, " Velocity: ", velocity, "m/s")        
        else:
            
            print("No object has passed the ",whichOSM(i),", analog signal is too small for Correlation")
            print("Max Set 1: ",max(set_1))
    
    return 

    
if __name__=="__main__":
    # plt.close('all')

    # filetype = 'txt' # 'h5', 'csv' or 'txt
    
    # available=glob.glob("data/*."+filetype)
        
    # for i in available:
    #     print(available.index(i)+1,i)
        
    # nuTxtFile=int((input("Number of file: ")))
    
    # nameTxtFile=available[nuTxtFile-1]
    # print("Chosen file: ",nameTxtFile)  
    global data
    global time
    global sampling_rate
    
    sampling_rate = 20e3
    timeStamp=1/sampling_rate

    
    if 0: # plot OSM responses when pulsed
        # '''Load Data'''
        # Rutsche-2022-06-14-09-47-33.h5 always on RGBW
        # Rutsche-2022-05-30-13-21-33.h5 blue
        # Rutsche-2022-06-14-10-01-09.h5 green
        # Rutsche-2022-06-14-09-53-49.h5 pulsed RGBW
        path=[ 'data/Rutsche-2022-06-14-10-03-36.h5','data/Rutsche-2022-06-14-10-01-09.h5',\
               'data/Rutsche-2022-06-14-09-47-33.h5','data/Rutsche-2022-06-14-09-53-49.h5']
        
        j = 0

            
        for i in path:

            data, daq_settings = load_DAQ_data(i)   
        
            '''define globals'''

            time = np.arange(0,timeStamp*len(data),timeStamp) 
            '''Plot OSM Voltage'''
            # OSMs are OSM 0, 2 , 4
            if j<2:
                xrange = [0,2.5]
                plotSingleOSMVoltage(data, OSM_number = 0, x_range = xrange,markevery =1000, nameOfFile = i)
            else:
                plotSingleOSMVoltage(data, OSM_number = 0, x_range = None,markevery = 500, nameOfFile = i)
            # plotSingleOSMVoltage(data, OSM_number = 2)
            j+=1
        
    if 1: # plot excerpt of always on OSM signal
        # Rutsche-2022-06-14-09-47-33.h5
        path = 'data/Rutsche-2022-06-14-09-47-33.h5'
        data, daq_settings = load_DAQ_data(path)
        
        '''define globals'''
        sampling_rate = 20e3
        timeStamp=1/sampling_rate
        time = np.arange(0,timeStamp*len(data),timeStamp)
        
        '''Plot OSM Voltage'''
        # OSMs are OSM 0, 2 , 4
        plotSingleOSMVoltage(data, OSM_number = 0, x_range = [0.5, 1], markevery =1000,nameOfFile = path, addString = '_excerpt')
        
    if 0: # check OSM signal
        # Rutsche-2022-05-31-12-28-24.h5
        path = 'data/Rutsche-2022-06-14-09-47-33.h5'
        data, daq_settings = load_DAQ_data(path)
        
        '''define globals'''
        sampling_rate = 20e3
        timeStamp=1/sampling_rate
        time = np.arange(0,timeStamp*len(data),timeStamp)
        
        '''Plot OSM Voltage'''
        # OSMs are OSM 0, 2 , 4
        plotSingleOSMVoltage(data, OSM_number = 0, x_range = None ,nameOfFile = path, addString = '_trash')
        
    
    if 0: 
        '''Plot point lasers'''
        nameTxtFile = 'data/Rutsche-2022-04-22-16-29-01.csv'
        data, daq_settings = load_DAQ_data(nameTxtFile)
        
        plotPointLaserHeight(data)
        
    
    '''Plot all Sensor responses'''
    # plot()
    
    if 0: # calculate velocity of OSM 
        # 'data/Rutsche-2022-06-14-09-47-33.h5'
        path = 'data/Rutsche-2022-06-14-09-47-33.h5'
        data, daq_settings = load_DAQ_data(path)
        
        '''define globals'''
        sampling_rate = 20e3
        timeStamp=1/sampling_rate
        time = np.arange(0,timeStamp*len(data),timeStamp)
        plotSingleOSMVoltage(data, OSM_number = 0, x_range = [0,2] ,nameOfFile = path, addString = '_trash')
        '''Calcualte cross correlation'''
        xcorr()
    
    
    
     
