# -*- coding: utf-8 -*-
"""
Created on Wed Mar 23 09:34:24 2022

@author: admin

Current Bugs:
    Timing not stable
    Maximal read of 1000 values
    
"""

import PySimpleGUI as gui
import nidaqmx
from nidaqmx.constants import TerminalConfiguration
from nidaqmx.constants import Edge
from nidaqmx.constants import AcquisitionType
import time
import numpy as np
import threading
import matplotlib.pyplot as plt

# %matplotlib qt # use in cmd line to show plots seperately
    
def MeasThread(DAQAI_task):
    OSM_1=[]
    value=[]
    while True:
        global stop_MeasThread
        value = DAQAI_task.read()
        # print(str(value)+"\n")
        OSM_1=np.append(OSM_1,value)
        if stop_MeasThread:
            
            plt.figure()
            plt.plot(OSM_1, label = "Voltage OSM Channel A")
            plt.xlabel('Time / s (currently false)')
            plt.ylabel('Voltage')
            plt.title('OSM_1 Channel A')
            plt.grid()
            plt.legend()
            plt.show()
            break
        
        
# check if device is available
system = nidaqmx.system.System.local()
system.driver_version

for device in system.devices:
        print(device) # plot devices

layout = [[gui.Text("Measurement")], [gui.Button("Start"),gui.Button("Cancel")]]
layout1 = [[gui.Text("Measurement")], [gui.Button("Stop")]]

# Create the window
window = gui.Window("Avalanche Chute", layout, size=(290, 80))
windowMeasStarted=gui.Window("Avalanche Chute", layout1, size=(290, 80))

# OSM_1=[]
# value=[]
stop_MeasThread=False
samples_per_channel=12000
sample_rate=20000

with nidaqmx.Task() as DAQAI_task:
 
    # add_ai_voltage_chan(physical_channel, name_to_assign_to_channel='', terminal_config=TerminalConfiguration.DEFAULT,
    # min_val=- 5.0, max_val=5.0, units=VoltageUnits.VOLTS, custom_scale_name='')
    AI = DAQAI_task.ai_channels.add_ai_voltage_chan("PCI6024e/ai0", terminal_config = TerminalConfiguration.RSE,\
                                                    min_val=- 5.0, max_val=5.0)
    # DAQAI_task.timing.cfg_samp_clk_timing(sample_rate,source="", sample_mode=AcquisitionType.CONTINUOUS) #onboard clock
    # DAQAI_task.triggers.start_trigger.cfg_dig_edge_start_trig(trigger_source = trig_name)
    DAQAI_task.timing.cfg_samp_clk_timing(sample_rate,source="")
    print("Analog Input Mode: ",AI.ai_term_cfg,\
          " Sampling rate: ",DAQAI_task.timing.samp_clk_rate, "\n",\
              AI.ai_term_cfg)
    #DAQAI_task.start()
    DAQAI_task.stop()
    # Create an event loop
    while True:
        event, values = window.read()
        if event == "Start" or event == gui.WIN_CLOSED:
            window.close()
            break
        
    thread=threading.Thread(target=MeasThread ,args =(DAQAI_task, ))
    thread.start()
        
    # for i in range(0,samples_per_channel):
    #     value = DAQAI_task.read()
    #     OSM_1=np.append(OSM_1,value)
            
    while True:

        event, values = windowMeasStarted.read()
        
        # check_if_busy=DAQAI_task.is_task_done()
        #if check_if_busy:
        # if True:
        # DAQAI_task.read_many_sample(value, number_of_samples_per_channel=nidaqmx.constants. READ_ALL_AVAILABLE, timeout=10.0)
            # value = DAQAI_task.read()
            # OSM_1=np.append(OSM_1,value)
        if event == "Stop" or event == gui.WIN_CLOSED:
                stop_MeasThread=True
                thread.join()
                windowMeasStarted.close()
                break
         
    # print(OSM_1)
    
    
 
# plt.figure()
# plt.plot(OSM_1, label = "Voltage OSM Channel A")
# plt.xlabel('Time / s (currently false)')
# plt.ylabel('Voltage')
# plt.title('OSM_1 Channel A')
# plt.grid()
# plt.legend()
# plt.show()


