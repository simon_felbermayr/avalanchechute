#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun  8 13:41:41 2022

@author: simon
"""



from openpiv import tools, pyprocess, scaling, filters, \
                    validation, preprocess
import numpy as np
from skimage import io
import matplotlib.pyplot as plt

file_a = '/home/simon/Documents/TestVideos/2022_05_12_TestswithStrobo/C001C0207_20220512150814_0001/02_UpperChute/frame1054.jpg'
file_b = '/home/simon/Documents/TestVideos/2022_05_12_TestswithStrobo/C001C0207_20220512150814_0001/02_UpperChute/frame1057.jpg'

im_a  = tools.imread( file_a )
im_b  = tools.imread( file_b )
plt.imshow(np.c_[im_a,im_b],cmap='gray')
plt.title('Original')

# let's crop the region of interest
frame_a =  im_a
frame_b =  im_b
plt.figure()
plt.imshow(np.c_[frame_a,frame_b],cmap='gray')

# Process the original cropped image and see the OpenPIV result:

# typical parameters:
window_size = 32 #pixels
overlap = 16 # pixels
search_area_size = 64 # pixels 
frame_rate = 20 # fps
scaling_factor = 1000 # micron/pixel



# masking using optimal (manually tuned) set of parameters and the right method:
masked_a, _ = preprocess.dynamic_masking(frame_a,method='edges',filter_size=7,threshold=0.01)
masked_b, _ = preprocess.dynamic_masking(frame_b,method='edges',filter_size=7,threshold=0.01)
plt.figure()
plt.imshow(np.c_[masked_a,masked_b],cmap='gray')
plt.title('Masked Image')



# Process the masked cropped image and see the OpenPIV result:

# process again with the masked images, for comparison# process once with the original images
u, v, sig2noise = pyprocess.extended_search_area_piv(
                                                       masked_a.astype(np.int32) , masked_b.astype(np.int32), 
                                                       window_size = window_size,
                                                       overlap = overlap, 
                                                       dt=1./frame_rate, 
                                                       search_area_size = search_area_size, 
                                                       sig2noise_method = 'peak2peak')
x, y = pyprocess.get_coordinates(masked_a.shape, 
                               search_area_size, 
                               overlap )
u, v, mask = validation.global_val( u, v, (-300.,300.),(-300.,300.))
u, v, mask = validation.sig2noise_val( u, v, sig2noise, threshold = 1.1)
u, v = filters.replace_outliers( u, v, method='localmean', max_iter = 3, kernel_size = 3)
x, y, u, v = scaling.uniform(x, y, u, v, scaling_factor = scaling_factor )
# save to a file
x, y, u, v = tools.transform_coordinates(x, y, u, v)
tools.save(x, y, u, v, mask, 'test_masked.txt', fmt='%9.6f', delimiter='\t')
tools.display_vector_field('test_masked.txt', scale=5, width=0.006)



