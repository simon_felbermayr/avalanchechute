# -*- coding: utf-8 -*-
"""
Created on Tue Mar 29 14:31:22 2022

@author: admin
"""

import PySimpleGUI as gui
import ClassChuteDAQ as ChuteDAQ
import ClassCamComm as CamComm
import ClassRelaisControl as ReControl
import threading

class GUI:
    
    def __init__(self):
        self.layout = [[gui.Text("Measurement")], [gui.Button("Start"),gui.Button("Cancel")]]
        self.layout1 = [[gui.Text("Measurement")], [gui.Button("Stop")]]
        
        # Create the window
        self.window = gui.Window("Avalanche Chute", self.layout, size=(290, 80))
        self.windowMeasStarted=gui.Window("Avalanche Chute", self.layout1, size=(290, 80))
        self.daq=ChuteDAQ.ChuteDAQ()
        self.cam=CamComm.CamComm()   
        self.rel=ReControl.Relai()
        self.rel.connect_Relai()
        
    def startSession(self, measurement_duration=10):
        
        while True:
            event, values = self.window.read()
            if event == "Start":
                self.window.close()  

                ''' TODO Insert function to open granulate inlet'''
                       
                        
                
                DAQThread=threading.Thread(target=self.daq.StartMeas)
                CamThread=threading.Thread(target=self.cam.Record)
                RelThread=threading.Thread(target=self.rel.openGranulateInlet)
                DAQThread.start()
                CamThread.start()
                RelThread.start()
                
                #self.cam.Record()                           # start recording with camera
                # self.daq.StartMeas()    # start analog data aquisition
                break
            elif event =="Cancel" or event == gui.WIN_CLOSED:
                self.window.close()
                #self.cam.disconnect_ZCam()
                break
            
        # while True:
        #     event, values = self.windowMeasStarted.read()
        #     if event == "Stop" or event == gui.WIN_CLOSED:
        #             self.windowMeasStarted.close()
        #             break
                
if __name__=="__main__":
    daq=GUI()
    daq.startSession()
    
