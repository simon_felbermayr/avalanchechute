# -*- coding: utf-8 -*-
"""
Created on Tue Mar 29 15:17:06 2022

@author: admin
"""
import datetime
import time
import serial
import UART_def
import sys
import glob
import numpy as np
import threading

class CamComm(threading.Thread):

    def __init__(self, measurement_duration=10):
        threading.Thread.__init__(self)
        """ Lists serial port names
            :raises EnvironmentError:
                On unsupported or unknown platforms
            :returns:
                A list of the serial ports available on the system
        """
        self.measurement_duration=measurement_duration
        if sys.platform.startswith('win'):
            ports = ['COM%s' % (i + 1) for i in range(256)]
        elif sys.platform.startswith('linux') or sys.platform.startswith('cygwin'):
            # this excludes your current terminal "/dev/tty"
            ports = glob.glob('/dev/tty[A-Za-z]*')
        elif sys.platform.startswith('darwin'):
            ports = glob.glob('/dev/tty.*')
        else:
            raise EnvironmentError('CAM: Unsupported platform')
    
        result = []
        for port in ports:
            try:
                s = serial.Serial(port)
                s.close()
                result.append(port)
            except (OSError, serial.SerialException):
                pass

        if sys.platform.startswith('win'):
            device ='COM3'
            exists=device in result
        elif sys.platform.startswith('linux') or sys.platform.startswith('cygwin'):
            device='/dev/ttyUSB0'
            exists=device in result
        
        print('CAM: Camera is available to USB port: '+str(exists))
        try:
            self.ser = serial.Serial(
            	port=device,
            	baudrate=115200,
                stopbits=serial.STOPBITS_ONE,
                bytesize=serial.EIGHTBITS,
                parity=serial.PARITY_NONE
            )
            print('CAM: Camera connected')
        except:
            print("CAM: Camera is not available try to disconnect and connect again")
            #if self.ser.isOpen():
            #    self.disconnect_ZCam(self.ser)
        
        
    def disconnect_ZCam(self):
        self.ser.close()
        
    def write_ZCam_common_request(self,numBytes,data):
        self.ser.write(serial.to_bytes([0xea,0x02,numBytes,data]))
        
    def startRec(self):
        self.write_ZCam_common_request(0x01,UART_def.UART_START_REC)
        print(datetime.datetime.now(),"CAM: Recording started")
        
    def stopRec(self):
        self.write_ZCam_common_request(0x01,UART_def.UART_STOP_REC)
        print(datetime.datetime.now(),"CAM: Recording stopped")
        
    def decreaseISO(self):
        self.ser.write(serial.to_bytes([0xea,0x02,0x03,0x2b,0x00,0x03]))
    
    def increaseISO(self):
        self.ser.write(serial.to_bytes([0xea,0x02,0x03,0x2c,0x00,0x03]))
        
    def Record(self):
        self.startRec()
        time_cam_start = time.perf_counter()
    
        
        
        while True:
            elapsed_time = np.floor(time.perf_counter() - time_cam_start)
            if elapsed_time > self.measurement_duration:
                self.stopRec()
                self.disconnect_ZCam()
                break
                
            
        
        

if __name__ == "__main__":
    cam=CamComm()
    cam.disconnect_ZCam()
    
    
# """    
#     Traceback (most recent call last):

#   File "C:\Users\admin\Documents\avalanchechute\00_DAQ\ClassGUI.py", line 62, in <module>
#     daq.startSession()

#   File "C:\Users\admin\Documents\avalanchechute\00_DAQ\ClassGUI.py", line 46, in startSession
#     self.cam.Record()                           # start recording with camera

#   File "C:\Users\admin\Documents\avalanchechute\00_DAQ\ClassCamComm.py", line 98, in Record
#     self.stopRec()

#   File "C:\Users\admin\Documents\avalanchechute\00_DAQ\ClassCamComm.py", line 80, in stopRec
#     self.write_ZCam_common_request(0x01,UART_def.UART_STOP_REC)

#   File "C:\Users\admin\Documents\avalanchechute\00_DAQ\ClassCamComm.py", line 73, in write_ZCam_common_request
#     self.ser.write(serial.to_bytes([0xea,0x02,numBytes,data]))

#   File "C:\Users\admin\anaconda3\lib\site-packages\serial\serialwin32.py", line 317, in write
#     raise SerialException("WriteFile failed ({!r})".format(ctypes.WinError()))

#    SerialException: WriteFile failed (OSError(9, 'Das Handle ist ungültig.', None, 6))
# """