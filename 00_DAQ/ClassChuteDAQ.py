# -*- coding: utf-8 -*-
"""
Created on Tue Mar 29 14:21:11 2022

@author: admin
"""

import PySimpleGUI as gui
import nidaqmx
from nidaqmx.constants import TerminalConfiguration
from nidaqmx.constants import Edge
from nidaqmx.constants import AcquisitionType
import time
import datetime
import numpy as np
import threading
import matplotlib.pyplot as plt
import itertools
import h5py

class ChuteDAQ(threading.Thread):
    
    def __init__(self,measurement_duration=10):
        threading.Thread.__init__(self)
        # definition of class attributes
        self.run_bool = True # should be controlled by GUI or caller thread
        self.max_log_rate = 200000
        self.measurement_duration = measurement_duration
        
        # check if device is available
        system = nidaqmx.system.System.local()
        system.driver_version
        
        for device in system.devices:
                print(device) # plot devices
        
        ADC_DEVICE_NAME = device.name # 'PCI6024e'
        
        
        print('ADC: init measure for', self.measurement_duration, 'seconds')
        
        # Setup ADC
        self.parameter = { 
            "channels": 8,  # number of AI channels
            "channel_name": ADC_DEVICE_NAME + '/ai0:7',
            # channels name, list: https://zone.ni.com/reference/en-XX/help/370466AH-01/mxcncpts/physchannames/
            "log_rate": int(20000),  # Samples per second
            "adc_min_value": -5.0,  # minimum ADC value in Volts
            "adc_max_value": 5.0,  # maximum ADC value in Volts
            "timeout": self.measurement_duration + 2.0,  # timeout to detect external clock on read
            "daq_read_sleep": 0,  # seconds, artifically slow down read loop -> bigger buffer
            "debug_output": 1,
        }
        
        # check log rate:
        if self.parameter['log_rate'] * self.parameter['channels'] > self.max_log_rate:
            print('ADC: For',  self.parameter['channels'],'channels, max log_rate is', \
                  int(self.max_log_rate / self.parameter['channels']), 'not', self.parameter['log_rate'])
            self.parameter['log_rate'] = int(self.max_log_rate / self.parameter['channels'])
            print('     --> Adjusted log rate to:', self.parameter['log_rate'])
            
        # set additional parameter which need values from the first.
        self.parameter["buffer_size"] = int(self.parameter["log_rate"])  # buffer size in samples
        self.parameter["measurement_duration"] = self.measurement_duration
        self.parameter["requested_samples"] = self.parameter["log_rate"] * self.measurement_duration # factor 10000 because threading causes more time than actual desired
        
        # update by self.parameter from input!
        #self.parameter.update(param)
        
        
        # set some useful variables
        self.n_samples = self.parameter["requested_samples"]  # total number of samples
        

        self.parameter["hdf5_write"] = True  # write in array


        #self.filename= time.strftime("data/%d%m%Y%H%M%S")+".txt"
        self.filename= time.strftime("data/Rutsche-%Y-%m-%d-%H-%M-%S")

        # pre-allocate array or create h5-file
        if self.parameter['hdf5_write']:
            self.filename += '.h5'
            # create a h5-file object if True
            self.f = h5py.File(self.filename, 'w')
            self.data = self.f.create_dataset('data', (0, self.parameter["channels"]), 
                                              maxshape=(None, self.parameter["channels"]), 
                                              chunks=True)
        else:
            self.filename += '.csv'
            # we get up to 1 buffer more than requested...
            self.data = np.empty((self.parameter["requested_samples"]+self.parameter["buffer_size"], self.parameter["channels"]), dtype=np.float64)
            self.data[:] = np.nan
            
        # with nidaqmx.Task() as task:
        self.task = nidaqmx.Task() 
        self.task.ai_channels.add_ai_voltage_chan(self.parameter["channel_name"],
                                             terminal_config=nidaqmx.constants.TerminalConfiguration.RSE,
                                             min_val=self.parameter["adc_min_value"],
                                             max_val=self.parameter["adc_max_value"],
                                             units=nidaqmx.constants.VoltageUnits.VOLTS
                                             )
    
        self.task.timing.cfg_samp_clk_timing(rate=self.parameter["log_rate"],
                                        sample_mode=nidaqmx.constants.AcquisitionType.CONTINUOUS,
                                        # sample_mode=nidaqmx.constants.AcquisitionType.FINITE,
                                        # samps_per_chan=n_samples
                                        )  # you may not need samps_per_chan
    
        # check clock settings
        if 0:
            print("ADC: clock_src:", self.task.timing.samp_clk_timebase_src)
            print("ADC: clock_rate:", self.task.timing.samp_clk_timebase_rate)
            print("ADC: clock_devider:", self.task.timing.samp_clk_timebase_div)
            print("ADC: sample clock rate:", self.task.timing.samp_clk_rate)
            print("ADC: MAX sample rate:", self.task.timing.samp_clk_max_rate)   
    
    
    def StartMeas(self):

        try:
            self.task.control(nidaqmx.constants.TaskMode.TASK_COMMIT)
            time_adc_start = time.perf_counter()
    
            # helper variables
            total_samples = 0
            i = 0
            last_display = -1
        
        
            self.parameter["acquisition_start"] = str(datetime.datetime.now())
            if 1:
                print("ADC:   ---   acquisition started:", self.parameter["acquisition_start"])
                print("ADC: Requested samples:", self.n_samples, "Acquisition duration:",
                              self.measurement_duration)
        
        
            # ############################# READING LOOP ##########################
            while self.run_bool and total_samples < self.n_samples and time.perf_counter() - time_adc_start < self.parameter[
                "timeout"]:
                i = i + 1
        
                if self.parameter["debug_output"] >= 1:
                    elapsed_time = np.floor(time.perf_counter() - time_adc_start)  # in sec
                    if elapsed_time != last_display:
                        print("ADC: ...", round(elapsed_time), "of", self.measurement_duration, "sec:",
                                      total_samples, "acquired ...")
                        last_display = elapsed_time
        
            
                # high-lvl read function: always create a new array
                data_buff = np.asarray(
                    self.task.read(number_of_samples_per_channel=nidaqmx.constants.READ_ALL_AVAILABLE)).T
                time_adc_end = time.perf_counter()
        
                samples_from_buffer = data_buff.shape[0]
        
                # get nr of samples and acumulate to total_samples
                total_samples = int(total_samples + samples_from_buffer)
                if self.parameter["debug_output"] >= 2:
                    print("ADC: iter", i, "total:", total_samples, "smp from buffer", samples_from_buffer,
                                  "time elapsed", time.perf_counter() - time_adc_start)
                if samples_from_buffer > 0:
                    if self.parameter["hdf5_write"]:  # sequential write to hdf5 file
                        chunk_start = self.data.shape[0]
                        # resize dataset in file
                        self.data.resize(self.data.shape[0] + samples_from_buffer, axis=0)
                        if self.parameter["debug_output"] >= 3:
                            print("hdf5 file shape:", self.data.shape, "buffer shape:", data_buff.shape)
                    else:
                        # sequential write in pre-allocated array 'self.data'
                        chunk_start = int(np.count_nonzero(~np.isnan(self.data)) / self.parameter["channels"])
                        if self.parameter["debug_output"] >= 3:
                            print("Non-empty data shape: (", chunk_start, self.data.shape[1],
                                          "), buffer shape:", data_buff.shape)
                    
                        if self.parameter['channels'] == 1:
                            data_buff = data_buff[:, np.newaxis]
                        
                    #print("Length: ",len(data_buff))
                        
                    # write to HDF5 file or into np array
                    self.data[chunk_start:chunk_start + samples_from_buffer, :] = data_buff
                    # self.data[chunk_start:chunk_start + len(data_buff), :] = data_buff
        
                if self.parameter["daq_read_sleep"] > 0:
                    time.sleep(self.parameter["daq_read_sleep"])

            self.parameter["acquisition_stop"] = str(datetime.datetime.now())
            # ############################# READING LOOP #########################
        finally:
            self.task.close()
        
        if self.parameter['hdf5_write']:
            print('ADC: wrote HDF5 file: '+self.filename)
        else:
            # save data-array to file
            # https://pythonexamples.org/python-numpy-save-and-load-array-from-file/
            self.data=np.where(self.data==0.,0.00,self.data)
            print("ADC: Write data to file: "+self.filename)
            c=np.savetxt(self.filename,self.data,delimiter=',')
            # with open(filename, 'a') as myfile:
            #     myfile.write(', '.join(str(item) for item in self.data))
            print("ADC: Write data to file finished")

        
        if self.parameter["debug_output"] >= 1:
            print("ADC: requested points:   ", self.n_samples)
            print("ADC: total aqcuired points", total_samples, "in", time_adc_end - time_adc_start)
            print("ADC:  ---   aqcuisition finished:", self.parameter["acquisition_stop"])
            
            print("ADC: sample rate:", round(1/((time_adc_end-time_adc_start)/self.parameter["requested_samples"])))
        
        # prepare data nparray for return
        if not self.parameter["hdf5_write"]:
            total_written = int(np.count_nonzero(~np.isnan(self.data)) / self.parameter["channels"])
            if self.parameter["debug_output"] >= 2:
                print("resize data array by cutting", self.data.shape[0] - total_written, "tailing NaN's")
            self.data = np.resize(self.data, (total_written, self.parameter["channels"]))
            if self.parameter["debug_output"] >= 1:
                print("ADC: data array size:", self.data.shape)
        
        # add more self.parameter to wrtie into the hdf5 file
        self.parameter["total_samples"] = total_samples
        self.parameter["total_acquisition_time"] = time_adc_end - time_adc_start
        self.parameter["data_shape"] = self.data.shape
        
        if self.parameter["hdf5_write"]:      
            hdf5_write_parameter(self.f, self.parameter) # write parameter
            self.f.close()
    
    
def hdf5_write_parameter(h5_file, parameter, group_name='parameter'):
    """
    write parameter dictionary to HDF5 file

    Parameters are written as a group called 'parameter'
    into the HDF5 file

    Parameters
    ----------
    h5_file : class.h5py
        HDF5 file opened with h5py.File(<filename>)
        TODO: change input to filename
    parameter : dict
        parameter dictionary to write into the file
    group_name : str
        usually just 'parameter' as groupname in hdf5 file

    Returns
    -------

    Note: from mgeodar.hdf5.mgeo_hdf5_write_dict
    """

    debug_str = "[hdf5_write_parameter]"

    # add parameter group
    param_grp = h5_file.create_group(group_name)
    # write single item
    for key, item in parameter.items():
        try:
            if item is None:
                item = 'None'
            if isinstance(item, dict):
                # recursive write each dictionary
                hdf5_write_parameter(h5_file, item, group_name+'/'+key)
            else:
                h5_file.create_dataset("/"+group_name+"/{}".format(key), data=item)
        except:
            print(debug_str+": failed to write:", key, "=", item)
    return


    
if __name__=="__main__":
    daq=ChuteDAQ(measurement_duration=5)
    daq.StartMeas()
