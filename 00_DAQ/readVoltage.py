# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

# use nidaqmx version 19.0 for PCI6024e

import nidaqmx
from nidaqmx.constants import TerminalConfiguration
from nidaqmx.constants import Edge
from nidaqmx.constants import AcquisitionType
import time
import numpy as np
import threading

meas=True
i=1
finished=0

def DAQ_read(DAQ_task):
    global i
    if meas:
        if i<100:
            i=i+1
            print("read: ",time.time_ns()) 
            value = DAQ_task.read(number_of_samples_per_channel=1)
            threading.Timer(0.00005, DAQ_read(DAQ_task)).start()
    else:
        print("Finished")
        
def measFinished():
    global finished, meas
    if meas:
        threading.Timer(0.00006, measFinished).start()
        if i>=100:
            meas=False
            finished=time.time()
            print("Meas stopped")
        
# https://stackoverflow.com/questions/56366033/continuous-acquistion-with-nidaqmx


# check if device is available
system = nidaqmx.system.System.local()
system.driver_version

for device in system.devices:
        print(device) # plot devices

with nidaqmx.Task() as DAQAI_task:
    samples_per_channel=10000

    AI = DAQAI_task.ai_channels.add_ai_voltage_chan("PCI6024e/ai0", terminal_config=TerminalConfiguration.RSE)

    DAQAI_task.timing.cfg_samp_clk_timing(20000,source="",\
                                          sample_mode=nidaqmx.constants.AcquisitionType.FINITE, \
                                          samps_per_chan=samples_per_channel)

    print("Analog Input Mode: ",AI.ai_term_cfg," Sampling rate: ",DAQAI_task.timing.samp_clk_rate, "\n")
    # DAQAI_task.start()
    
    DAQAI_task.control(nidaqmx.constants.TaskMode.TASK_START)
    
    i = 0
    tic = time.perf_counter() #_ns()*10e-9
    # DAQ_read(DAQAI_task)
    # measFinished()
    value = DAQAI_task.read(samples_per_channel)
    
    DAQAI_task.control(nidaqmx.constants.TaskMode.TASK_STOP)

    toc = time.perf_counter() #_ns()*10e-9
    print("Tic: ",tic,"Toc: ",toc, "Frequency: ", round(1/((toc-tic)/(samples_per_channel))))
    
    # while True:
    #     check_if_busy=DAQAI_task.is_task_done()
    #     if not check_if_busy:
    #         value = DAQAI_task.read()
        
    #     toc = time.time()
    #     if i==0:
    #         Result = np.array([[(toc-tic)],[value]])
    #     else:
    #         Result = np.append(Result, [[(toc-tic)],[value]],axis=1)
    #     if Result[0,i] >= 100:
    #         print("break reached")
    #         break
    #     else:
    #         i = i +1
    
 


