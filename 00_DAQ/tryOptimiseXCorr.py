import glob
import numpy as np
from ast import literal_eval
import matplotlib.pyplot as plt
from scipy import signal
import statsmodels.api as sm
from scipy.signal import correlate
import statistics
import warnings as w
import math

'''
ToDo Thoughts:
    maybe use padding for windows to prevent cutting data set 2
'''

plt.close('all')

global data
#global data
global sampling_rate
data = np.loadtxt("data/31032022141628.txt", delimiter=',', dtype=float)
if np.isnan(data).any():
    data=data[~np.isnan(data).any(axis=1), :]

maxVel=10 #m/s
sensor_gap=11e-3 # m
sampling_rate=20e3
timeStamp=1/sampling_rate
time = np.arange(0,timeStamp*len(data),timeStamp)


def whichOSM(i):
         switcher={
                0:"OSM1",
                2:"OSM2",
                4:"OSM3"
                }
         return switcher.get(i,"Invalid counter variable")
     
        
def plot():
    fig, ax = plt.subplots(2, 4, sharex='all', sharey='all')
    ax[0][0].plot(time,data[:,0])
    ax[1][0].plot(time,data[:,1])
    ax[0][1].plot(time,data[:,2])
    ax[1][1].plot(time,data[:,3])
    ax[0][2].plot(time,data[:,4])
    ax[1][2].plot(time,data[:,5])
    ax[0][3].plot(time,data[:,6])
    ax[1][3].plot(time,data[:,7])
    plt.setp(ax, ylim=(-5,5))
    




#############################
# Overall calculation of cross correlation
# delete all nans

    
fig, ax1 = plt.subplots(1, 3, sharex='all', sharey='all')
j=0 #counter variable for plot
corr=[]
array=[]



    
for i in range(0,5,2):
    set_1=data[:,i+1]
    set_2=data[:,i]
    threshold_OSM=0.05
    
    # check data if a object passed the OSM
    if max(set_1)>threshold_OSM and max(set_2)>threshold_OSM:
        # cross correlation
        correlate_result=np.correlate(set_1, set_2,'full')
        
        maxCorr=np.amax(correlate_result)
        
        index_maxCorr = np.where(correlate_result == np.amax(correlate_result))
        time_lag = (index_maxCorr[0]-len(set_1))/sampling_rate
        velocity=sensor_gap/time_lag
        
        # plt.figure()
        # plt.plot(correlate_result)
        # plt.grid()
    
        if velocity>maxVel:
            print("Velocity exits maximum of "+ str(maxVel))
        else:
            ax1[j].plot(correlate_result)
            j=j+1
            
        ####################################
        # new approach to calc cross correlation with maxLag, threshold and max velocity
        buffer=500
        numbWindows=3
        
        posSensReachSet1    = next(x for x, val in enumerate(set_1)if val > threshold_OSM)
        posSensLeftSet1     = posSensReachSet1+next(x for x, val in enumerate(set_1[posSensReachSet1:])if val < threshold_OSM)
        
        posSensReachSet2 = next(x for x, val in enumerate(set_2)if val > threshold_OSM)
        posSensLeftSet2= posSensReachSet2+next(x for x, val in enumerate(set_2[posSensReachSet2:])if val < threshold_OSM)
        
        posSensReachSet1=posSensReachSet1-buffer
        posSensLeftSet1=posSensLeftSet1+buffer
        posSensReachSet2=posSensReachSet2-buffer
        posSensLeftSet2=posSensLeftSet2+buffer
        
        lengthWindow=math.ceil((posSensLeftSet1-posSensReachSet1)/numbWindows)
        
        # have a look at the window size
        
        
        
        plt.figure()
        plt.plot(time,set_1)
        plt.axvline(posSensReachSet1*timeStamp)
        plt.axvline(posSensLeftSet1*timeStamp)
        plt.axvline((posSensReachSet1+lengthWindow)*timeStamp)
        plt.figure()
        plt.plot(time,set_2)
        plt.axvline(posSensReachSet2*timeStamp)
        plt.axvline(posSensLeftSet2*timeStamp)
        
        #set_1_corrWindow=data[posSensReachSet1-startBuff:posSensLeftSet1+lengthWindow*numbWindows] 
        # print(len(set_1_corrWindow))

       
        
        
        for m in range(0,numbWindows):
            currentWindowStartSet1=(posSensReachSet1+lengthWindow*m)
            currentWindowEndSet1=currentWindowStartSet1+lengthWindow
            
            for k in range(0,numbWindows):
                currentWindowStartSet2  =   (posSensReachSet2+lengthWindow*k)
                currentWindowEndSet2    =   currentWindowStartSet2+lengthWindow
                corr                    =   (np.correlate(data[currentWindowStartSet1:currentWindowEndSet1,i],data[currentWindowStartSet2:currentWindowEndSet2,i],'full'))
                array=np.append(array,corr)
                        
            plt.figure()
            plt.plot(array)
            print(len(array))
            plt.grid()
            array=[]
                
            
        print(whichOSM(i), "Time lag: ", time_lag, " Velocity: ", velocity)        
    else:
        print("No object has passed the ",whichOSM(i),", analog signal is too small for Correlation")
        
