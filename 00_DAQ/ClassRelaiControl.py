#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 31 17:05:23 2022

@author: simon
"""

import time
import serial
import UART_def
import sys
import glob
import threading

class Relai:
    def __init__(self):
        threading.Thread.__init__(self)
        # check if port is available and give back feedback which port is connected
        """ Lists serial port names
            :raises EnvironmentError:
                On unsupported or unknown platforms
            :returns:
                A list of the serial ports available on the system
        """
        if sys.platform.startswith('win'):
            ports = ['COM%s' % (i + 1) for i in range(256)]
        elif sys.platform.startswith('linux') or sys.platform.startswith('cygwin'):
            # this excludes your current terminal "/dev/tty"
            ports = glob.glob('/dev/tty[A-Za-z]*')
        elif sys.platform.startswith('darwin'):
            ports = glob.glob('/dev/tty.*')
        else:
            raise EnvironmentError('Unsupported platform')
        
        self.result = []
        for port in ports:
            try:
                s = serial.Serial(port)
                s.close()
                self.result.append(port)
            except (OSError, serial.SerialException):
                pass
            print(self.result)
        # return self.result
    
    def connect_Relai(self):
        
        if sys.platform.startswith('win'):
            device ='COM4'
            exists=device in self.serial_ports()
        elif sys.platform.startswith('linux') or sys.platform.startswith('cygwin'):
            device='/dev/ttyUSB0'
            exists=device in self.serial_ports()
        print('Relai is:'+str(exists))
        try:
            self.ser = serial.Serial(
            	port=device,
            	baudrate=9600,
                stopbits=serial.STOPBITS_ONE,
                bytesize=serial.EIGHTBITS,
                parity=serial.PARITY_NONE
            )
            print('Relai connected')
            # return self.ser
        except:
            print("Camera is not available try to disconnect and connect again")
            #if ser.isOpen():
            #    disconnect_ZCam(ser)
 
    def openGranulateInlet(self):
        self.connectTerminalAR1()
        self.connectTerminalBR1()
        self.disconnect_Relai()
        
    def disconnect_Relai(self):
        self.ser.close()
        
    # code for relai DLP-IOR4
    # to switch off terminal switch on other terminal (A / B)
    ##################
    # Relay 1
    def connectTerminalAR1(self):
        self.ser.write(serial.to_bytes([0x31]))
        
    def connectTerminalBR1(self):
        self.ser.write(serial.to_bytes([0x51]))
    
    ##################
    # Relay 2
    def connectTerminalAR2(self):
        self.ser.write(serial.to_bytes([0x32]))
        
    def connectTerminalBR2(self):
        self.ser.write(serial.to_bytes([0x57]))
        
    ##################
    # Relay 3
    def connectTerminalAR2(self):
        self.ser.write(serial.to_bytes([0x33]))
        
    def connectTerminalBR2(self):
        self.ser.write(serial.to_bytes([0x45]))
    
    ##################
    # Relay 4
    def connectTerminalAR2(self):
        self.ser.write(serial.to_bytes([0x34]))
        
    def connectTerminalBR2(self):
        self.ser.write(serial.to_bytes([0x52]))

if __name__ == "__main__":

    relai=Relai()
    relai.connect_Relai()
    relai.disconnect_Relai()